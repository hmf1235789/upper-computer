#include "formrequester.h"
#include "ui_formrequester.h"

FormRequester::FormRequester(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormRequester)
{
    ui->setupUi(this);
}

FormRequester::~FormRequester()
{
    delete ui;
}
