﻿#ifndef PROGRESSREPORT_H
#define PROGRESSREPORT_H

#include <QObject>

class NotificationManager : public QObject
{
    Q_OBJECT
private:
    NotificationManager(QObject *parent = nullptr);
    NotificationManager(const NotificationManager&)=delete;
    NotificationManager& operator=(const NotificationManager&)=delete;

public:

    QDateTime GetMeasureStartTime();
    QDateTime GetLastEachMeasureStartTime();
    QList<QDateTime> EachMeasureStartTime;

    static NotificationManager& Instance();

    void AddBaseProgress(double weight);

    void SetCurrentFlow(QString name);

    void UpdateProgress(double progress);

    void SetSpectrumSavePath(QString path);

    void Reset();

    int CurrentMeasureIndex;

signals:
    void OnReport(QString title,int progress);

public slots:

private:
    QString _currentFlow;

    QString _spectrumPath="";
    QList<double> _weights;
};

#endif // PROGRESSREPORT_H
