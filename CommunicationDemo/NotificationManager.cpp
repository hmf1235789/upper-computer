﻿#include "NotificationManager.h"
#include "GlobalVar.h"

NotificationManager::NotificationManager(QObject *parent) : QObject(parent)
{
    _currentFlow="--";
}

QDateTime NotificationManager::GetMeasureStartTime()
{
    if(EachMeasureStartTime.count()!=0)
    {
        return EachMeasureStartTime.at(0);
    }
    else
    {
        return QDateTime::currentDateTime();
    }
}

QDateTime NotificationManager::GetLastEachMeasureStartTime()
{
    if(EachMeasureStartTime.count()!=0)
    {
        return EachMeasureStartTime.last();
    }
    else
    {
        return QDateTime::currentDateTime();
    }
}


NotificationManager& NotificationManager::Instance()
{
    static NotificationManager _instance;
    return _instance;
}

void NotificationManager::AddBaseProgress(double weight)
{
    _weights.append(weight);
}

void NotificationManager::UpdateProgress(double progress)
{
    double baseProgress=0;
    for(int i=0;i<_weights.count()-1;i++)
    {
        baseProgress+=_weights.at(i);
    }
    double cur=_weights.last();
    int curProgress=baseProgress+cur*progress/100;
    emit OnReport(_currentFlow,curProgress);
}

void NotificationManager::SetSpectrumSavePath(QString path)
{
    _spectrumPath=path;
}

void NotificationManager::SetCurrentFlow(QString name)
{
    _currentFlow=name;
}


void NotificationManager::Reset()
{
    _weights.clear();
    _currentFlow="--";
}
