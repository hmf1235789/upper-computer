﻿#include "DialogConfig.h"
#include "ui_DialogConfig.h"


DialogConfig::DialogConfig(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogConfig)
{
    ui->setupUi(this);
    LoadConfig();
}

DialogConfig::~DialogConfig()
{
    delete ui;
}

void DialogConfig::on_btnLoad_clicked()
{
    LoadConfig();
}

void DialogConfig::on_btnSave_clicked()
{
    ControlParam p=GetControlParam();
    GlobalVar::Instance().SetControlParam(p);
}

void DialogConfig::LoadConfig()
{
    ControlParam para=GlobalVar::Instance().GetControlParam();
    ui->spinTakeSample->setValue(para.TakeSampleDurationSecond);
    ui->spinTakeSamplePump->setValue(para.TakeSamplePumpDurationSecond);
    ui->spinInWater->setValue(para.InWaterDurationSecond);
    ui->spinAutoIn->setValue(para.AutoInSampleDurationSecond);
    ui->spinManualIn->setValue(para.ManualInSampleDurationSecond);
    ui->spinInSamplePump->setValue(para.InSamplePumpDurationSecond);
    ui->spinOutSample->setValue(para.OutSampleDurationSecond);
    ui->spinBlow->setValue(para.BlowDurationSecond);
}

ControlParam DialogConfig::GetControlParam()
{
    ControlParam para;
    para.TakeSampleDurationSecond = ui->spinTakeSample->value();
    para.TakeSamplePumpDurationSecond = ui->spinTakeSamplePump->value();
    para.InWaterDurationSecond = ui->spinInWater->value();
    para.AutoInSampleDurationSecond = ui->spinAutoIn->value();
    para.ManualInSampleDurationSecond = ui->spinManualIn->value();
    para.InSamplePumpDurationSecond = ui->spinInSamplePump->value();
    para.OutSampleDurationSecond = ui->spinOutSample->value();
    para.BlowDurationSecond = ui->spinBlow->value();
    return para;
}
