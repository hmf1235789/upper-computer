﻿#ifndef BASESENDREPLY_H
#define BASESENDREPLY_H
#include <QObject>
#include <QTcpSocket>
#include <QTimer>
#include <QSemaphore>

#include "BaseCommunication.h"
#include "DialogSendReply.h"

//定义发送接收模式对象的必要功能 请求恢复 请求无回复 数据接收
//客户端和服务端功能不一样 服务端只接受响应 客户端负责发送接收数据
class BaseSendReply:public BaseCommunication
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit BaseSendReply(QObject *parent=nullptr);

public:
    virtual bool SendReply1(char sendBuff[],int size,QByteArray &receiveBuff);
    virtual bool SendReply(QByteArray sendBuff,QByteArray &receiveBuff);
    bool SendReplyJson(QJsonObject sendBuff,QJsonObject &receiveBuff);

    virtual bool SendNoReply(QByteArray sendBuff);
    bool SendNoReply1(char sendBuff[],int size);
    bool SendNoReplyJson(QJsonObject obj);

signals:
    void DataReceive(QByteArray recData);
    // BaseCommunication interface
public:
    bool Connect() override;
    bool IsConnected() override;

private:
    QTcpSocket *_socket=nullptr;
    QString _ipPort="";
    QTimer* _timer=nullptr;
    QDialog* _configDialog=nullptr;

    // BaseCommunication interface
public:
    QWidget *GetConfigWidget() override;

    static QString HexToString(QByteArray data);
};

#endif // BASESENDREPLY_H
