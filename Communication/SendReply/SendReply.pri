INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/BaseSendReply.h \
    $$PWD/DialogSendReply.h \
    $$PWD/TcpSendReplyClient.h \
    $$PWD/SerialPortSendReplyClient.h

SOURCES += \
    $$PWD/BaseSendReply.cpp \
    $$PWD/DialogSendReply.cpp \
    $$PWD/TcpSendReplyClient.cpp \
    $$PWD/SerialPortSendReplyClient.cpp

FORMS += \
    $$PWD/DialogSendReply.ui
