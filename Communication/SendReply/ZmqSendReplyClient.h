﻿#ifndef ZMQSENDREPLY_H
#define ZMQSENDREPLY_H
#include "BaseSendReply.h"
#include "zmq.h"

class ZmqSendReplyClient:public BaseSendReply
{
    Q_OBJECT
public:
    Q_INVOKABLE ZmqSendReplyClient();

    // BaseCommunication interface
public:
    bool Connect() override;

    // BaseSendReply interface
public:
    bool SendReply(QByteArray sendBuff, QByteArray &receiveBuff) override;

private:
    //  Socket to talk to clients
    void *_context;
    void *_socket;

    // BaseCommunication interface
public:
    QDialog *GetConfigWidget() override;

    // BaseCommunication interface
public:
    void Dispose() override;
};

#endif // ZMQSENDREPLY_H
