﻿#include "BaseSendReply.h"


BaseSendReply::BaseSendReply(QObject *parent):BaseCommunication(parent)
{

}

bool BaseSendReply::SendReply1(char sendBuff[], int size, QByteArray &receiveBuff)
{
    QByteArray arr(sendBuff,size);
    return SendReply(arr,receiveBuff);
}

bool BaseSendReply::SendNoReply(QByteArray sendBuff)
{
    Q_UNUSED(sendBuff);
    return false;
}

bool BaseSendReply::SendNoReply1(char sendBuff[], int size)
{
    QByteArray arr(sendBuff,size);
    return SendNoReply(arr);
}

bool BaseSendReply::SendNoReplyJson(QJsonObject obj)
{
    QJsonDocument doc(obj);
    return SendNoReply(doc.toJson());
}

bool BaseSendReply::Connect()
{
    return true;
}

bool BaseSendReply::IsConnected()
{
    return true;
}

QWidget *BaseSendReply::GetConfigWidget()
{
    DialogSendReply* _configDialog=new DialogSendReply();
    _configDialog->SetSendReply(this);
    return _configDialog;
}

QString BaseSendReply::HexToString(QByteArray data)
{
    QString hexString = data.toHex();
    QString formattedString;
    for (int i = 0; i < hexString.length(); i += 2) {
        formattedString +=  hexString.mid(i, 2) + " ";
    }
    return formattedString;
}

bool BaseSendReply::SendReply(QByteArray sendBuff, QByteArray &receiveBuff)
{
    qDebug()<<__FUNCTION__<<__LINE__<<HexToString(sendBuff);
    Q_UNUSED(sendBuff);
    Q_UNUSED(receiveBuff);
    return true;
}

bool BaseSendReply::SendReplyJson(QJsonObject sendBuff, QJsonObject &receiveBuff)
{
    QJsonDocument doc(sendBuff);
    QByteArray data;
    if(SendReply(doc.toJson(),data))
    {
//        qDebug()<<__FUNCTION__<<__LINE__<<data;
        QJsonParseError parseError;
        QJsonDocument jsonDoc = QJsonDocument::fromJson(data, &parseError);
        if (parseError.error != QJsonParseError::NoError) {
            qDebug() << "JSON parse error:" << parseError.errorString();
            return false;
        }
        receiveBuff=jsonDoc.object();
        return true;
    }
    return false;
}
