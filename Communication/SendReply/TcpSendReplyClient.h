﻿#pragma execution_character_set("utf-8")
#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QTimer>
#include <QSemaphore>
#include <QMutex>
#include <QMutexLocker>

#include "BaseCommunication.h"
#include "BaseSendReply.h"

//QSemaphore Semaphore(1);

class TcpSendReplyClient :public BaseSendReply
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit TcpSendReplyClient(QObject *parent=nullptr);
    ~TcpSendReplyClient();

    bool Connect() override;
    bool IsConnected() override;
    void Dispose() override;
    void SetOtherConfig(QJsonObject obj) override;
    //核心逻辑
    bool SendReply(QByteArray sendBuff,QByteArray &receiveBuff);
    bool SendNoReply(QByteArray sendBuff);

public slots:
    void OnError1(QAbstractSocket::SocketError err);

private:
    QTcpSocket *_socket=nullptr;
    QString _ipPort="";
    QTimer* _timer=nullptr;
    int _timeout=0;
    QMutex mutex;
    QList<QByteArray> _recList;

    void Reconnect();
    bool DisConnect();
    bool SendReplyImpl(QByteArray sendBuff,QByteArray &receiveBuff,int timeoutMs=1000);

private slots:
    bool HeadrBeat();
};

#endif // TCPCLIENT_H
