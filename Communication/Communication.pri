INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/tcpmonitor.h \
    $$PWD/BaseCommunication.h \
    $$PWD/CommunicationManager.h \
    $$PWD/IPUtils.h


SOURCES += \
    $$PWD/tcpmonitor.cpp \
    $$PWD/BaseCommunication.cpp \
    $$PWD/CommunicationManager.cpp

FORMS +=

include("./SendReply/SendReply.pri")
include("./CommunicationFactory/CommunicationFactory.pri")
include("./modbus/modbus.pri")
include("./req_rep/req_rep.pri")
include("./ModbusServer/ModbusServer.pri")
include("./SubScriber/SubScriber.pri")
