﻿#include "BaseCommunicationFactory.h"

#include <QDebug>

#include "BaseModbus.h"
#include "QSerialPortModbus.h"
#include "SerialModbus.h"
#include "QSerialPortModbus.h"
#include "QTcpModbus.h"
#include "TcpSendReplyClient.h"
#include "TcpModbus.h"
#include "SendReply/SerialPortSendReplyClient.h"
#include "SerialPortSendReplyClient.h"
#include "TcpModbusServer.h"


BaseCommunicationFactory::BaseCommunicationFactory()
{
    Register<BaseSendReply>();
    Register<TcpSendReplyClient>();
    Register<TcpModbus>();
    Register<QTcpModbus>();
    Register<QSerialPortModbus>();
    Register<SerialModbus>();
    Register<SerialPortSendReplyClient>();
    Register<TcpModbusServer>();
}
