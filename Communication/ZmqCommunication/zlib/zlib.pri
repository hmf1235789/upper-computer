INCLUDEPATH  += $$PWD/include

win32 {
    LIBS += -L$$PWD/dynamic/

    LIBS += -lzdll
    zlib.path = $$DESTDIR
    zlib.files +=  $$PWD/dynamic/zlib1.dll

    INSTALLS += zlib
}

unix {
    LIBS += -L$$PWD/lib/
    LIBS += -lz
}

HEADERS += \
    $$PWD/include/zlib.h \
    $$PWD/include/zconf.h

