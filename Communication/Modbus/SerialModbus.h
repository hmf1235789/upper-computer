﻿#ifndef SERIALNMODBUS_H
#define SERIALNMODBUS_H
#include <QJsonObject>

#include "BaseModbus.h"

class SerialModbus:public BaseModbus
{
    Q_OBJECT
public:
     Q_INVOKABLE explicit SerialModbus();

    // CommonModbus interface
public:
    void CreateModbusInstance() override;
};

#endif // SERIALNMODBUS_H
