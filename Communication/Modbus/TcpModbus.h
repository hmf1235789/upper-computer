﻿#ifndef TCPMODBUS_H
#define TCPMODBUS_H

#include "BaseModbus.h"

class TcpModbus:public BaseModbus
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit TcpModbus();

    // BaseModbus interface
public:
    void CreateModbusInstance() override;

};

#endif // TCPMODBUS_H
