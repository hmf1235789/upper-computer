INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/BaseModbus.h \
    $$PWD/TcpModbus.h \
    $$PWD/SerialModbus.h

SOURCES += \
    $$PWD/BaseModbus.cpp \
    $$PWD/TcpModbus.cpp \
    $$PWD/SerialModbus.cpp \

include("./libmodbus/libmodbus.pri")

DISTFILES +=
