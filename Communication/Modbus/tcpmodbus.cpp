﻿#include "TcpModbus.h"

TcpModbus::TcpModbus()
{

}

void TcpModbus::CreateModbusInstance()
{
    const QUrl url = QUrl::fromUserInput(_communicationParam);
    m_ctx=modbus_new_tcp(url.host().toLatin1().data(),url.port());
}
