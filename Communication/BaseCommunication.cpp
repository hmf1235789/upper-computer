﻿#include "BaseCommunication.h"

BaseCommunication::BaseCommunication(QObject *parent):BaseItem(parent)
{

}

void BaseCommunication::SetCommunicationParam(QString communicationParam)
{
    _communicationParam=communicationParam;
}

QString BaseCommunication::GetCommunicationParam()
{
    return _communicationParam;
}

bool BaseCommunication::Connect()
{
    return true;
}

bool BaseCommunication::IsConnected()
{
    return true;
}

void BaseCommunication::Dispose()
{

}

QWidget* BaseCommunication::GetConfigWidget()
{
    QDialog *w=new QDialog();
    return w;
}

void BaseCommunication::SetConfig(QJsonObject obj)
{
    BaseItem::SetConfig(obj);
    if(obj["Address"].isString())
    {
        SetCommunicationParam(obj["Address"].toString());
    }
    else
    {
        QJsonDocument jsonDoc(obj["Address"].toObject());
        QString jsonString = jsonDoc.toJson(QJsonDocument::Compact);
        SetCommunicationParam(jsonString);
    }
}

bool BaseCommunication::Init()
{
    return Connect();
}
