INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/BaseSubscriber.h \
    $$PWD/DialogSubscriber.h

SOURCES += \
    $$PWD/BaseSubscriber.cpp \
    $$PWD/DialogSubscriber.cpp

FORMS += \
    $$PWD/DialogSubscriber.ui
