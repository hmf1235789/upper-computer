﻿#ifndef DIALOGSUBSCRIBER_H
#define DIALOGSUBSCRIBER_H

#include <QDialog>
#include "BaseSubscriber.h"

namespace Ui {
class DialogSubscriber;
}

class DialogSubscriber : public QDialog
{
    Q_OBJECT

public:
    explicit DialogSubscriber(QWidget *parent = 0);
    ~DialogSubscriber();
    void SetCommunication(BaseSubscriber *subscriber);

    void AddNewLine(QString line);
private slots:
    void on_btnPublish_clicked();
    void OnDataReceive(QByteArray data);

private:
    Ui::DialogSubscriber *ui;
    BaseSubscriber *_subscriber=nullptr;
};

#endif // DIALOGSUBSCRIBER_H
