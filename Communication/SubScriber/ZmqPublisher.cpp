﻿#include "ZmqPublisher.h"

ZmqPublisher::ZmqPublisher(QObject *parent):BaseSubscriber(parent)
{

}

bool ZmqPublisher::Publish(QByteArray data)
{
    int dataLength=data.length();

    if(zmq_send(_socket,data.data(),dataLength,0)!=dataLength)
    {
        return false;
    }
    else
    {
        return true;
    }
}

bool ZmqPublisher::Connect()
{
    _context=zmq_ctx_new();
    _socket=zmq_socket(_context,ZMQ_PUB);
    int rc=zmq_bind(_socket,_communicationParam.toUtf8());
    if(rc!=0)
    {
        return false;
    }
//    int timeoutMs=3000;
//    zmq_setsockopt(_socket,ZMQ_RCVTIMEO,&timeoutMs,sizeof(timeoutMs));
    return true;
}

void ZmqPublisher::Dispose()
{
    zmq_close (_socket);
    zmq_ctx_destroy (_context);
}


