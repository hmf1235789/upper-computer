﻿#ifndef ZMQSUBSCRIBER_H
#define ZMQSUBSCRIBER_H

#include <QObject>
#include <QThread>
#include "BaseSubscriber.h"

class ZmqSubscriber :public virtual BaseSubscriber
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit ZmqSubscriber(QObject *parent = nullptr);

    virtual bool Publish(QByteArray data) override;

protected:
    void run();

signals:

public slots:

    // ICommunication interface
public:
    bool Connect();
    bool IsConnected();
    void Dispose();

private:
    bool initSUBModuleConnect();

    void * _context = NULL;     //订阅模式的ZMQ上下文环境
    void * _socket = NULL;

    bool isRun=false;
};

#endif // ZMQSUBSCRIBER_H
