﻿#ifndef ZMQPUBLISHER_H
#define ZMQPUBLISHER_H
#include "BaseCommunication.h"
#include "BaseSubscriber.h"

class ZmqPublisher:public BaseSubscriber
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit ZmqPublisher(QObject *parent = 0);
    virtual bool Publish(QByteArray data) override;

    // BaseCommunication interface
public:
    bool Connect() override;
    void Dispose() override;

private:
    //  Prepare our context and publisher
    void *_context =nullptr;
    void *_socket =nullptr;
};

#endif // ZMQPUBLISHER_H
