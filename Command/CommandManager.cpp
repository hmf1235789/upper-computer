﻿#include "CommandManager.h"
#include <QDebug>

#include "JsonUtils.h"
#include "DeviceManager.h"
#include "BaseCommandFactory.h"


CommandManager::CommandManager(QJsonObject obj, BaseFactory *factory, QObject *parent)
{
    _obj=obj;
    _factory=factory;
}

CommandManager::~CommandManager()
{

}

void CommandManager::InitConfig()
{
    CommandManager::InitImpl(_obj);
}

void CommandManager::InitImpl(QJsonObject obj)
{
    QString managerName=ManagerName();
    QJsonArray arr=obj["Commands"].toArray();
    if(_factory==nullptr)
    {
        throw QString("%1 未配置 工厂实例").arg(managerName);
    }

    for(QJsonValueRef it:arr)
    {
        QJsonObject value=it.toObject();
        QString key=value["Name"].toString();
        BaseItem *baseItem=_factory->CreateInstance(value["Type"].toString());
        baseItem->setParent(this);
        baseItem->SetName(key);
        baseItem->SetConfig(value);
        baseItem->SetOtherConfig(value["OtherConfig"].toObject());
        qDebug()<<baseItem->Name()<<"Init "<<baseItem->Init();
        _allItems.append(baseItem);
    }
    qDebug()<<managerName<<__LINE__<<ManagerName()<<" "<<_allItems.count()<<"Item";
    qDebug()<<"\r\n";
}



