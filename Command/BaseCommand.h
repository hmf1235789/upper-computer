﻿#ifndef BASECOMMAND_H
#define BASECOMMAND_H
#include <QObject>
#include <QJsonObject>
#include <QDebug>
#include "BaseDevice.h"
#include "BaseItem.h"
#include "DeviceManager.h"

class BaseCommand:public BaseItem
{
    Q_OBJECT
public:
    Q_INVOKABLE BaseCommand(QObject *parent=nullptr);

public:

protected:
    QList<BaseDevice*> _allDevices;

public:

public:
    virtual void Execute();

    QString GetFriendlyName();

    virtual void SetDevices(QList<BaseDevice *> devices);

    virtual QList<BaseCommand*> GetSubCommands();

    // BaseItem interface
public:
    virtual void SetConfig(QJsonObject obj) override;

    virtual int GetWeight();
};


#endif // BASECOMMAND_H
