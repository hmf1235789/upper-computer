﻿#ifndef CONSOLEUTILS_H
#define CONSOLEUTILS_H
#include "Head.h"
#include <QObject>
#include <QMap>
#include <QVariant>

class ConsoleUtils
{
public:
    ConsoleUtils();

    static QStringList GetStringList(int argc,char *argv[]);

    static QMap<QString,QString> GetArgument(QStringList args);

    static QString GetJoinString(QMap<QString,QVariant> map, QStringList headers);

};

#endif // CONSOLEUTILS_H
