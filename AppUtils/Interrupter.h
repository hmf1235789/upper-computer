﻿#ifndef INTERRUPTER_H
#define INTERRUPTER_H
#include "Head.h"
#include <QObject>

class Interrupter : public QObject
{
    Q_OBJECT
public:
    explicit Interrupter(QObject *parent = nullptr);

    void WaitMs(int time);
    void Reset();
    void Stop();

private:
    bool _isRunning;
    void CheckIsRunning();


signals:

public slots:
};

#endif // INTERRUPTER_H
