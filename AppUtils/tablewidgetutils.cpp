﻿#include "tablewidgetutils.h"
#include <QHeaderView>
#include <QDebug>

TableWidgetUtils::TableWidgetUtils()
{

}

QMap<QString, QList<double> > TableWidgetUtils::GetTableDataByColumn(const QTableWidget *tableWidget, int startColumnIndex)
{
    QList<int> columnIndex;
    for(int i=startColumnIndex;i<tableWidget->columnCount();i++)
    {
        columnIndex.append(i);
    }
    return GetTableDataByColumnImpl(tableWidget,columnIndex);
}

QMap<QString, QList<double> > TableWidgetUtils::GetTableDataByColumn(const QTableWidget *tableWidget, QList<int> notIncludeIndex)
{
    QList<int> columnIndex;
    for(int i=0;i<tableWidget->columnCount();i++)
    {
        if(!notIncludeIndex.contains(i))
        {
            columnIndex.append(i);
        }
    }
    return GetTableDataByColumnImpl(tableWidget,columnIndex);
}

QMap<QString, QList<double> > TableWidgetUtils::GetTableDataByColumnImpl(const QTableWidget *tableWidget, QList<int> columnIndex)
{
    QMap<QString, QList<double>> dataMap;

    int rowCount = tableWidget->rowCount();

    for(int column:columnIndex)
    {
        QString columnName = tableWidget->horizontalHeaderItem(column)->text();
        QList<double> columnData;

        for (int row = 0; row < rowCount; ++row) {
            QTableWidgetItem* item = tableWidget->item(row, column);
            if (item != nullptr) {
                bool ok;
                double value = item->text().toDouble(&ok);
                if (ok) {
                    columnData.append(value);
                }
            }
        }
        dataMap.insert(columnName, columnData);
    }
    return dataMap;
}

StringMat TableWidgetUtils::GetMat(const QTableWidget *tableWidget, QList<int> notIncludeIndex)
{
    // 创建一个QList<QList<QString>>来存储转换后的数据
    QList<QList<QString>> dataList;
    QList<QString> headerData;
    for (int col = 0; col < tableWidget->columnCount(); ++col)
    {
        if(!notIncludeIndex.contains(col))
        {
            headerData.append(tableWidget->horizontalHeaderItem(col)->text());
        }
    }
    dataList.append(headerData);

    // 遍历QTableWidget的行和列，并提取数据到dataList中
    for (int row = 0; row < tableWidget->rowCount(); ++row) {
        QList<QString> rowData;
        for (int col = 0; col < tableWidget->columnCount(); ++col) {
            if(!notIncludeIndex.contains(col))
            {
                QTableWidgetItem *item = tableWidget->item(row, col);
                if (item) {
                    rowData.append(item->text());
                } else {
                    rowData.append(""); // 添加空字符串，以保持数据对齐
                }
            }
        }
        dataList.append(rowData);
    }
    return dataList;
}

void TableWidgetUtils::SetContent(QStandardItemModel &model,Qt::Alignment alignment)
{
    for(int i = 0; i < model.rowCount(); i++) { //设置内容居中显示
        for(int j = 0; j < model.columnCount();j++) {
            if(model.item(i,j)) {
                model.item(i,j)->setTextAlignment(alignment);
            }
        }
    }
}

void TableWidgetUtils::SetHeader(QTableWidget *tableWidget,QStringList header)
{
    tableWidget->clear();
    tableWidget->setColumnCount(header.count());
    tableWidget->setRowCount(0);
    tableWidget->setHorizontalHeaderLabels(header);
    tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    tableWidget->setAlternatingRowColors(true);
//    SetHeaderWidth(tableWidget);
}

//void TableWidgetUtils::SetHeadersWidth(QTableWidget *tableWidget,QList<int> widths)
//{
//    tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
//     tableWidget->horizontalHeader()->se
//    tableWidget->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Fixed);
//    tableWidget->setColumnWidth(0,180);
//}

void TableWidgetUtils::SetHeaderWidth(QTableWidget *tableWidget)
{
    tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    tableWidget->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Fixed);
    tableWidget->setColumnWidth(0,200);
}

void TableWidgetUtils::SetTableWidgetWidth(QTableWidget *tableWidget,QMap<int,int> map)
{
    for(int idx:map.keys())
    {
        int w=map[idx];
        tableWidget->horizontalHeader()->setSectionResizeMode(idx,QHeaderView::Fixed);
        tableWidget->setColumnWidth(idx,w);
    }
}

void TableWidgetUtils::AddItem(QTableWidget *tableWidget, QMap<QString, QVariant> map, QStringList header)
{
    if(tableWidget->horizontalHeader()->count()!=header.count())
    {
        SetHeader(tableWidget,header);
    }
//    tableWidget->horizontalHeader()->setSectionResizeMode(1,QHeaderView::ResizeToContents);
    // 获取当前行数并插入一行
    int newRowIndex = tableWidget->rowCount();
    tableWidget->insertRow(newRowIndex);
    for(int i=0;i<header.count();i++)
    {
        QString key=header.at(i);
        QTableWidgetItem *newItem1 = new QTableWidgetItem();

        if(map.contains(key))
        {
            newItem1->setData(Qt::DisplayRole,QVariant(map[key]));
        }
        else
        {
            newItem1->setData(Qt::DisplayRole,QVariant("--"));
        }
        newItem1->setTextAlignment(Qt::AlignCenter);
        tableWidget->setItem(newRowIndex,i,newItem1);
    }
    tableWidget->show();
}
