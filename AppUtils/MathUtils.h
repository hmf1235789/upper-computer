﻿#ifndef MATHUTILS_H
#define MATHUTILS_H

#include <QList>
#include <QPointF>
#include <vector>
#include <string>
#include <algorithm>
#include <QString>
#include <QVector>
#include <vector>
using namespace std;
class MathUtils
{
public:
    MathUtils();

    static QList<double> LinearInterpolation(QList<double> xAxis,const QList<QPointF>& dataPoints);

    static vector<double> LoadMat2(string filename, int &row, int &column);

    static vector<vector<double>> LoadMat(string filename);

    static vector<int> FindIndex(vector<string> currentElement, vector<string> allElement);

    static QList<int> FindIndex(QList<QString> currentElement, QList<QString> allElement);

    static vector<string> Split(string source, char splitChar);

    static vector<double> ModifyXs(vector<double> oldXs, int itemLength, vector<double> ks, vector<double> bs, vector<int> modifyIndex);

    static double CalcCorrelation(vector<double> x,vector<double> y);

    static double CalcR2(vector<double> x,vector<double> y);

    static QVector<QVector<QString>> ReadCsvFile(const QString& filePath);

    static QVector<QVector<QString>> SwapRowColumn(const QVector<QVector<QString>>& matrix);

    static QVector<double> ConvertToDoubleVector(const QVector<QString>& vec);


private:
    static double Average(vector<double> values);

    static double linearInterpolation(double x_interpolate, const QList<QPointF>& dataPoints);
};

#endif // MATHUTILS_H
