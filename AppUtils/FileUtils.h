﻿#ifndef FILEUTILS_H
#define FILEUTILS_H
#include "Head.h"
#include <QString>
#include <QDateTime>
#include <QDir>
#include <fstream>

class FileUtils
{
public:
    FileUtils();

    static QString CreateDirFromDatetime(QString baseFloder,QDateTime dt);

    static bool TryCreateDir(QString folder);

    static bool ExportToCsv(QMap<QString,QList<double>> data,QString fileName);

    static bool ExportToCsv(QList<QList<QString>> data,QString fileName);

    static QList<QList<QString> > ReadCsvFile(const QString &filePath);

    static QList<QList<QString> > TransposeMatrix(const QList<QList<QString> > &mat);

    static QList<QString> listFilesRecursively(const QDir &dir);

    static QList<QString> listFilesRecursively(const QDir &dir,QString filter);

    //filter "*.db"
    static QList<QString> ListFiles(const QDir &dir,QString filter);

    static QList<QString> Filter(QList<QString> fileNames, QString filter);

    static QList<QString> Filter(QList<QString> fileNames, QDateTime start,QDateTime end);

    static QString ReadFile(QString fn);

    static void ExportToFile(QString content,QString fn);

    static void CopyFileZZZ(const QString &sourceFilePath, const QString &destinationFilePath);

    static void ClearFile(QString fn,int remainGB);

    static int GetRemainGB(QString dir);
};

#endif // FILEUTILS_H
