﻿#include "QssUtils.h"
#include <QElapsedTimer>
#include <QTextStream>
#include <QFile>
#include <QApplication>
#include <QPalette>
#include <QDebug>

QssUtils::QssUtils(QObject *parent) : QObject(parent)
{

}

void QssUtils::LoadStyle(QString fn)
{
    //开启计时
    QElapsedTimer time;
    time.start();

    //加载样式表
    QString qss;
    QFile file(fn);
    if (file.open(QFile::ReadOnly)) {
        //用QTextStream读取样式文件不用区分文件编码 带bom也行
        QStringList list;
        QTextStream in(&file);
        //in.setCodec("utf-8");
        while (!in.atEnd()) {
            QString line;
            in >> line;
            list << line;
        }

        file.close();
        qss = list.join("\n");
//        QString paletteColor = qss.mid(20, 7);
//        qApp->setPalette(QPalette(paletteColor));
        //用时主要在下面这句
        qApp->setStyleSheet(qss);
    }

    qDebug() << "用时:" << time.elapsed();
}
