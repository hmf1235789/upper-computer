INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/Head.h \
    $$PWD/ConsoleUtils.h \
    $$PWD/JsonUtils.h \
    $$PWD/FileUtils.h \
    $$PWD/MathUtils.h \
    $$PWD/BitUtils.h \
    $$PWD/CrcUtils.h \
    $$PWD/QProcessPro.h \
    $$PWD/Crypto.h \
    $$PWD/HtmlExport.h \
    $$PWD/apputils.h \
    $$PWD/QssUtils.h \
    $$PWD/listutils.h \
    $$PWD/qstringutils.h \
    $$PWD/quihelper.h \
    $$PWD/quihelperdata.h \
    $$PWD/tablewidgetutils.h \
    $$PWD/WaitUtils.h \
    $$PWD/widgetutils.h \
    $$PWD/Interrupter.h \
    $$PWD/customtablewidget.h \
    $$PWD/logger.h \
    $$PWD/TimerManager.h

SOURCES += \
    $$PWD/ConsoleUtils.cpp \
    $$PWD/JsonUtils.cpp \
    $$PWD/FileUtils.cpp \
    $$PWD/MathUtils.cpp \
    $$PWD/BitUtils.cpp \
    $$PWD/CrcUtils.cpp \
    $$PWD/QProcessPro.cpp \
    $$PWD/Crypto.cpp \
    $$PWD/HtmlExport.cpp \
    $$PWD/apputils.cpp \
    $$PWD/QssUtils.cpp \
    $$PWD/listutils.cpp \
    $$PWD/qstringutils.cpp \
    $$PWD/quihelper.cpp \
    $$PWD/quihelperdata.cpp \
    $$PWD/tablewidgetutils.cpp \
    $$PWD/WaitUtils.cpp \
    $$PWD/widgetutils.cpp \
    $$PWD/Interrupter.cpp \
    $$PWD/TimerManager.cpp

DISTFILES +=
