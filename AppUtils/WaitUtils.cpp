﻿#pragma execution_character_set("utf-8")

#include "WaitUtils.h"
#include <QTime>
#include <QDebug>
#include <QCoreApplication>
#include <QElapsedTimer>

WaitUtils::WaitUtils(QObject *parent) : QObject(parent)
{

}

bool WaitUtils::_isRunning=false;


void WaitUtils::WaitMs(int time)
{
    //多处调用displayProgress该变量且有的时true有的时false时会导致进度显示异常，想不通。。。
    QElapsedTimer ela;
    ela.start();
    while (ela.elapsed() < time)
    {
        CheckIsRunning();
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }
}

void WaitUtils::WaitMsNoException(int msec)
{
    //多处调用displayProgress该变量且有的时true有的时false时会导致进度显示异常，想不通。。。
    QElapsedTimer ela;
    ela.start();
    while (ela.elapsed() < msec)
    {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 300);
    }
}

void WaitUtils::Reset()
{
    _isRunning=true;
}

void WaitUtils::Stop()
{
    _isRunning=false;
}

void WaitUtils::WaitMsNoProgress(int time)
{
    QElapsedTimer ela;
    ela.start();
    while (ela.elapsed() < time)
    {
        CheckIsRunning();
        QCoreApplication::processEvents(QEventLoop::AllEvents, 300);
    }
}

void WaitUtils::CheckIsRunning()
{
    if(!_isRunning)
    {
        throw QString("流程中止");
    }
}

