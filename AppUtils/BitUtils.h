﻿#ifndef BITUTILS_H
#define BITUTILS_H

#include <QList>

class BitUtils
{
public:
    BitUtils();

    static QList<uint16_t> ConvertToUint16List(int data);

    static int ConvertToInt(QList<uint16_t> data);

    static QString HexToString(QByteArray data);

    static QList<uint8_t> ConvertToUint8List(uint16_t data);
    static QList<uchar> Uint32ConvertToUCharList(uint32_t data);

    static QList<uint8_t> Uint32ConvertToUint8List(uint32_t data);
    //求和校验
    static QList<uchar> SumCheck(QList<uchar> data);
    static bool GetBit(uint16_t b, int bitNumber);
    static uint16_t GetNum(const QVector<bool> &bArr);

    static float ArrayToFloat(QByteArray data);
    static QByteArray FloatToArray(float value);

    static int ArrayToInt(QByteArray data);
    static QByteArray IntToArray(int value);
};

#endif // BITUTILS_H
