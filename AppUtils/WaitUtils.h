﻿#ifndef CMDWAIT_H
#define CMDWAIT_H

#include <QObject>

class WaitUtils : public QObject
{
    Q_OBJECT
public:
    explicit WaitUtils(QObject *parent = nullptr);
//    CmdWait(const CmdWait)
    static void WaitMs(int time);
    static void Reset();
    static void Stop();
    static void WaitMsNoProgress(int time);
    static void WaitMsNoException(int msec);

private:
    static bool _isRunning;

    static void CheckIsRunning();

public slots:
};

#endif // CMDWAIT_H
