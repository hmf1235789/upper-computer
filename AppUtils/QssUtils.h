﻿#ifndef QSSUTILS_H
#define QSSUTILS_H
#include "Head.h"
#include <QObject>

class QssUtils : public QObject
{
    Q_OBJECT
public:
    explicit QssUtils(QObject *parent = nullptr);

    static void LoadStyle(QString fn);

signals:

public slots:
};

#endif // QSSUTILS_H
