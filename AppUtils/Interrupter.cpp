﻿#include <QElapsedTimer>
#include <QCoreApplication>
#include "Interrupter.h"

Interrupter::Interrupter(QObject *parent) : QObject(parent)
{
    _isRunning=true;
}

void Interrupter::WaitMs(int time)
{
    //多处调用displayProgress该变量且有的时true有的时false时会导致进度显示异常，想不通。。。
    QElapsedTimer ela;
    ela.start();
    while (ela.elapsed() < time)
    {
        CheckIsRunning();
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }
}

void Interrupter::Reset()
{
    _isRunning=true;
}

void Interrupter::Stop()
{
    _isRunning=false;
}

void Interrupter::CheckIsRunning()
{
    if(!_isRunning)
    {
        throw QString("流程中止");
    }
}
