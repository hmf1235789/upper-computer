﻿#ifndef CUSTOMTABLEWIDGET_H
#define CUSTOMTABLEWIDGET_H
#include <QTableWidget>
#include <QKeyEvent>
#include <QClipboard>
#include <QHeaderView>

class CustomTableWidget : public QTableWidget
{
public:
    CustomTableWidget(QWidget *parent = nullptr) : QTableWidget(parent) {
//        this->horizontalHeader()->setStyleSheet("QHeaderView::section { border-bottom: 1px solid black; }");
    }

protected:
    void keyPressEvent(QKeyEvent *event) override
    {
        if (event->matches(QKeySequence::Copy)) {

            QModelIndexList selected = selectedIndexes();
            QString copiedText;
            int currentRow = -1;

            if (!selected.isEmpty())
            {
                for (const QModelIndex &index : selected) {
                    if (index.row() != currentRow) {
                        if (!copiedText.isEmpty()) {
                            copiedText += '\n';
                        }
                        currentRow = index.row();
                    } else {
                        copiedText += '\t';
                    }

                    copiedText += index.data().toString();
                }

                QClipboard *clipboard = QGuiApplication::clipboard();
                clipboard->setText(copiedText);
            }
        }

        QTableWidget::keyPressEvent(event);
    }
};
#endif // CUSTOMTABLEWIDGET_H
