﻿#pragma execution_character_set("utf-8")
#ifndef TABLEWIDGETUTILS_H
#define TABLEWIDGETUTILS_H

#include <QMap>
#include <QList>
#include <QTableWidget>
#include <QStandardItemModel>

using StringMat=QList<QList<QString>>;

class TableWidgetUtils
{
public:
    TableWidgetUtils();

    static QMap<QString, QList<double>> GetTableDataByColumn(const QTableWidget* tableWidget,int startColumnIndex);
    static QMap<QString, QList<double>> GetTableDataByColumn(const QTableWidget* tableWidget,QList<int> notIncludeIndex);
    static QMap<QString, QList<double>> GetTableDataByColumnImpl(const QTableWidget* tableWidget,QList<int> columnIndex);

    static StringMat GetMat(const QTableWidget* tableWidget,QList<int> notIncludeIndex);

    static void  SetContent(QStandardItemModel &model,Qt::Alignment alignment);

    static void SetHeader(QTableWidget *tableWidget, QStringList header);

    static void AddItem(QTableWidget *tableWidget, QMap<QString, QVariant> map, QStringList header);

    static void SetTableWidgetWidth(QTableWidget *tableWidget, QMap<int, int> map);

private:
    static void SetHeaderWidth(QTableWidget *tableWidget);
};

#endif // TABLEWIDGETUTILS_H
