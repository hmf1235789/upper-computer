﻿#include "ConsoleUtils.h"

ConsoleUtils::ConsoleUtils()
{

}

QStringList ConsoleUtils::GetStringList(int argc, char *argv[])
{
    QStringList ret;
    for (int i = 1; i < argc; ++i)
    {
        ret.append(QString::fromUtf8(argv[i]));
    }
    return ret;
}

QMap<QString, QString> ConsoleUtils::GetArgument(QStringList args)
{
    QMap<QString,QString> ret;
    for(int i=0;i<args.count();i++)
    {
        QString argument = args.at(i);
        int equalIndex = argument.indexOf('=');
        if (equalIndex != -1) {
            QString key = argument.left(equalIndex);
            QString value = argument.mid(equalIndex + 1);
            ret.insert(key,value);
        }
    }
    return ret;
}

QString ConsoleUtils::GetJoinString(QMap<QString, QVariant> map, QStringList headers)
{
    QStringList ret;
    for(int i=0;i<headers.count();i++)
    {
        QVariant itVar=map[headers.at(i)];
        if(itVar.type()==QVariant::Double)
        {
            ret.append(QString::number(itVar.toDouble()));
        }
        else if(itVar.type()==QVariant::String)
        {
            ret.append(itVar.toString());
        }
    }
    return ret.join("\t");
}
