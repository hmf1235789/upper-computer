﻿#ifndef INTERRUPTEDEXCEPTION_H
#define INTERRUPTEDEXCEPTION_H

#include <QObject>
#include <QException>

class InterruptedException : public QException
{
public:
    void raise() const override;
    InterruptedException *clone() const override;
};

#endif // INTERRUPTEDEXCEPTION_H
