INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/BaseManager.h \
    $$PWD/BaseItem.h \
    $$PWD/BaseFactory.h \
    $$PWD/InterruptedException.h \
    $$PWD/GlobalEnum.h

SOURCES += \
    $$PWD/BaseManager.cpp \
    $$PWD/BaseItem.cpp \
    $$PWD/BaseFactory.cpp \
    $$PWD/InterruptedException.cpp
