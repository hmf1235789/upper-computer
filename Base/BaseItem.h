﻿#ifndef BASEITEM_H
#define BASEITEM_H
#include "Head.h"
#include <QObject>
#include <QJsonObject>
#include <QJsonDocument>
#include <QException>
#include <QSharedPointer>
#include <QDebug>
#include "InterruptedException.h"

class BaseItem : public QObject
{
    Q_OBJECT
public:
    explicit BaseItem(QObject *parent = nullptr);

    QString Name();
    void SetName(QString name);

    virtual void SetConfig(QJsonObject obj);
    virtual QJsonObject Config();

    virtual void SetOtherConfig(QJsonObject other);

    virtual bool Init();

    virtual QWidget* GetConfigWidget();

    virtual QSharedPointer<QWidget> GetConfigWidgetZZZ();

protected:
    QString _name;
    QJsonObject _obj;
    QJsonObject _other;

signals:

public slots:
};

#endif // BASEITEM_H
