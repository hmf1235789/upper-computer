﻿#ifndef INSAMPLECOMMAND_H
#define INSAMPLECOMMAND_H
#include "Head.h"
#include <QObject>
#include "BaseIOModule.h"
#include "BaseCommand.h"
#include "NotificationManager.h"

class InSampleCommand : public BaseCommand
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit InSampleCommand(QObject *parent = nullptr);

signals:

public slots:

    // BaseCommand interface
public:
    virtual void Execute() override;
    virtual void SetDevices(QList<BaseDevice *> devices) override;
};

#endif // INSAMPLECOMMAND_H
