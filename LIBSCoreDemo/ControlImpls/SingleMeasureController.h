﻿#ifndef SINGLEMEASURECONTROLLER_H
#define SINGLEMEASURECONTROLLER_H
#include "Head.h"
#include "BaseController.h"

//单次测量
class SingleMeasureController:public BaseController
{
    Q_OBJECT
public:
    Q_INVOKABLE SingleMeasureController(QObject *parent=nullptr);

    // BaseController interface
public:
    virtual void Execute() override;

    virtual void SetConfig(QJsonObject obj) override;
};

#endif // SINGLEMEASURECONTROLLER_H
