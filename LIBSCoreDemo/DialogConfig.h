﻿#ifndef DIALOGCONFIG_H
#define DIALOGCONFIG_H
#include "Head.h"
#include <QDialog>
#include "GlobalVar.h"

namespace Ui {
class DialogConfig;
}

class DialogConfig : public QDialog
{
    Q_OBJECT

public:
    explicit DialogConfig(QWidget *parent = 0);
    ~DialogConfig();

private slots:
    void on_btnLoad_clicked();

    void on_btnSave_clicked();

private:
    Ui::DialogConfig *ui;

    void LoadConfig();
    ControlParam GetControlParam();
};

#endif // DIALOGCONFIG_H
