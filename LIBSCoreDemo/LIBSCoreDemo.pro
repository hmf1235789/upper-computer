#-------------------------------------------------
#
# Project created by QtCreator 2024-07-22T10:45:39
#
#-------------------------------------------------

QT       += core gui serialport sql network serialbus printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

include($$PWD/LiquidSensor/LiquidSensor.pri)
include($$PWD/ControlImpls/ControlImpls.pri)
include($$PWD/CommandImpls/CommandImpls.pri)

include($$PWD/../Base/Base.pri)
include($$PWD/../Communication/Communication.pri)
include($$PWD/../Device/Device.pri)
include($$PWD/../AppUtils/AppUtils.pri)
include($$PWD/../Customplot/Customplot.pri)
include($$PWD/../Command/Command.pri)
include($$PWD/../Control/Control.pri)

TARGET = LIBSCoreDemo
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

#指定编译生成的文件到temp目录 分门别类存储
MOC_DIR     = temp/moc
RCC_DIR     = temp/rcc
UI_DIR      = temp/ui
OBJECTS_DIR = temp/obj

DESTDIR += $$PWD/bin_vs

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    DialogConfig.cpp \
    DialogStyle.cpp \
    GlobalVar.cpp \
    NotificationManager.cpp

HEADERS += \
        mainwindow.h \
    DialogConfig.h \
    DialogStyle.h \
    GlobalVar.h \
    NotificationManager.h

FORMS += \
        mainwindow.ui \
    DialogConfig.ui \
    DialogStyle.ui

DISTFILES += \
    myico.rc \
    log.ini

RESOURCES += \
    source.qrc
