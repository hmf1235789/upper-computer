﻿#include "XKCLiquidSensor.h"

XKCLiquidSensor::XKCLiquidSensor(QObject *parent) : BaseLiquidSensor(parent)
{

}

void XKCLiquidSensor::SetCommunication(QList<BaseCommunication *> communication)
{
    BaseDevice::SetCommunication(communication);

    if(communication.empty())
    {
        throw QString("XKCLiquidSensor 未配置通讯实体！");
    }
    _modbus=dynamic_cast<BaseModbus*>(communication.first());
}

void XKCLiquidSensor::GetConnectStateCore(DeviceStateItems &items)
{
    _state=GetState();
    if(items.count()>0)
    {
        items[0].Data=QString::number((int)(_state.State));
    }
}

LiquidState XKCLiquidSensor::GetState()
{
    QList<uint16_t> retList;
    _modbus->ReadOutRegisters(1,2,retList);
    LiquidState ret;
    ret.State=retList.at(0);
    ret.RSSI=retList.at(1);
    return ret;
}
