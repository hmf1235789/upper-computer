INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/BaseLiquidSensor.h \
    $$PWD/XKCLiquidSensor.h \
    $$PWD/DialogLiquidState.h

SOURCES += \
    $$PWD/BaseLiquidSensor.cpp \
    $$PWD/XKCLiquidSensor.cpp \
    $$PWD/DialogLiquidState.cpp

FORMS += \
    $$PWD/DialogLiquidState.ui
