﻿#include "GlobalVar.h"
#include "JsonUtils.h"

GlobalVar::GlobalVar(QObject *parent):QObject(parent)
{
    QJsonObject mainObj=JsonUtils::LoadMainConfig();
    DeviceType=mainObj["DeviceType"].toString();

    QString initFile=QString("./Config/%1/%2").arg(DeviceType,"ClientConfig.ini");
    Setting=new QSettings(initFile,QSettings::IniFormat);
    Setting->setValue("sample","-----");
    Setting->sync();
}

GlobalVar &GlobalVar::Instance()
{
    static GlobalVar _instance;
    return _instance;
}


ControlParam GlobalVar::GetControlParam()
{
    QSettings *set=Setting;
    if(!set->contains("Control/TakeSampleDurationSecond"))
    {
        ControlParam setParam;
        SetControlParam(setParam);
    }

    ControlParam ret;
    set->beginGroup("Control");
    ret.TakeSampleDurationSecond=Setting->value("TakeSampleDurationSecond",0).toInt();
    ret.TakeSamplePumpDurationSecond=Setting->value("TakeSamplePumpDurationSecond",0).toInt();
    ret.InWaterDurationSecond=Setting->value("InWaterDurationSecond",0).toInt();
    ret.AutoInSampleDurationSecond=Setting->value("AutoInSampleDurationSecond",0).toInt();
    ret.ManualInSampleDurationSecond=Setting->value("ManualInSampleDurationSecond",0).toInt();
    ret.InSamplePumpDurationSecond=Setting->value("InSamplePumpDurationSecond",0).toInt();
    ret.OutSampleDurationSecond=Setting->value("OutSampleDurationSecond",0).toInt();
    ret.BlowDurationSecond=Setting->value("BlowDurationSecond",0).toInt();
    set->endGroup();
    return ret;
}

void GlobalVar::SetControlParam(ControlParam param)
{
    QSettings *set=Setting;
    set->beginGroup("Control");
    set->setValue("TakeSampleDurationSecond",param.TakeSampleDurationSecond);
    set->setValue("TakeSamplePumpDurationSecond",param.TakeSamplePumpDurationSecond);
    set->setValue("InWaterDurationSecond",param.InWaterDurationSecond);
    set->setValue("AutoInSampleDurationSecond",param.AutoInSampleDurationSecond);
    set->setValue("ManualInSampleDurationSecond",param.ManualInSampleDurationSecond);
    set->setValue("InSamplePumpDurationSecond",param.InSamplePumpDurationSecond);
    set->setValue("OutSampleDurationSecond",param.OutSampleDurationSecond);
    set->setValue("BlowDurationSecond",param.BlowDurationSecond);
    set->endGroup();
    set->sync();
}
