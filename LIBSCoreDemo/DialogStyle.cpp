﻿#include <QApplication>
#include <QStyleFactory>

#include "DialogStyle.h"
#include "ui_DialogStyle.h"

DialogStyle::DialogStyle(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogStyle)
{
    ui->setupUi(this);
    ui->comboBox->addItems(QStyleFactory::keys());
}

DialogStyle::~DialogStyle()
{
    delete ui;
}

void DialogStyle::on_pushButton_clicked()
{
    QString style=ui->comboBox->currentText();
    qApp->setStyle(QStyleFactory::create(style));
}
