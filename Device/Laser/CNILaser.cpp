﻿#include <QElapsedTimer>
#include <QCoreApplication>
#include <QThread>
#include <QJsonDocument>
#include "CNILaser.h"
#include "CrcUtils.h"
#include "ethlasercmd.h"
#include "WaitUtils.h"
#include "bitutils.h"

CNILaser::CNILaser(QObject *parent):BaseLaser(parent)
{

}

CNILaser::~CNILaser()
{
}

bool CNILaser::writeMsg(QByteArray sendbuf, QByteArray &recvData)
{
    unsigned int incrc16 = CRCUtils::CRC16_Check(reinterpret_cast<unsigned char *>(sendbuf.data()),sendbuf.count());
    QList<char> crc=CRCUtils::Parase(incrc16);
    for(auto item:crc)
    {
        sendbuf.append(item);
    }
    if(_sendReply==nullptr)
    {
        throw QString("Laser 未配置通讯!");
    }
    if(!_sendReply->SendReply(sendbuf,recvData))
    {
        qDebug()<<"data send error";
        return false;
    }
    else
    {
        return true;
    }
}

void CNILaser::waitMoment(int msec)
{
    WaitUtils::WaitMsNoException(msec);
}

 //光闸开关
int CNILaser::LaserShutterSwitch(bool bopen)
{
    int iErrorCode = 0;
    QByteArray sendBuf=CreateControlCmd(0x20,bopen);
    QByteArray recvBuf;
    iErrorCode = writeMsg(sendBuf,recvBuf);
    return iErrorCode;
}

//使能开关
int CNILaser::LaserEnableSwitch(bool bopen)
{
    int iErrorCode = 0;
    QByteArray sendBuf=CreateControlCmd(0x21,bopen);
    QByteArray recvBuf;
    iErrorCode = writeMsg(sendBuf,recvBuf);
    return iErrorCode;
}


QByteArray CNILaser::CreateControlCmd(int funCode,int enable)
{
    QByteArray sendBuf("\x7f\x05");
    sendBuf.append(static_cast<char>(funCode));
    sendBuf.append(BitUtils::IntToArray(enable));
    return sendBuf;
}

void CNILaser::Open()
{
    LaserShutterSwitch(true);
    waitMoment();
    LaserEnableSwitch(true);
}

void CNILaser::Close()
{
    if(_sendReply->IsConnected())
    {
        LaserEnableSwitch(false);
        waitMoment();
        LaserShutterSwitch(false);
    }
}

void CNILaser::SetFlashlampSyncMode(LASER_SYNC_MODE mode)
{
    int val = (mode==LASER_SYNC_MODE::LASER_FLASHLAMP_SYNC_INTERNAL)?0:1;
    QByteArray sendBuf=CreateControlCmd(0x02,val);
    QByteArray recvBuf;
    writeMsg(sendBuf,recvBuf);
}

void CNILaser::SetFrequence(int freq)
{
    QByteArray sendBuf("\x7f\x05\x02");
    float dfreq = freq;
    sendBuf.append(BitUtils::FloatToArray(dfreq));
    QByteArray recvBuf;
    writeMsg(sendBuf,recvBuf);
}

void CNILaser::SetEnergy(int level)
{
    QByteArray sendBuf("\x7f\x05\x023");
    qDebug()<<__FUNCTION__<<__LINE__<<BitUtils::IntToArray(level).toHex();
    sendBuf.append(BitUtils::IntToArray(level));
    QByteArray recvBuf;
    writeMsg(sendBuf,recvBuf);
}

bool CNILaser::GetStatus(QJsonObject &obj)
{
    QByteArray sendBuf("\x5d\x01\x04");
    QByteArray recvBuf;
    if( writeMsg(sendBuf,recvBuf))
    {
        // 将 QJsonObject 序列化为 JSON 字符串
        QByteArray data=EthLaserCmd::GetData(recvBuf);
        obj=EthLaserCmd::GetSystemStatus(data);
        return true;
    }
    return false;
}

void CNILaser::SetCommunication(QList<BaseCommunication *> communication)
{
    BaseDevice::SetCommunication(communication);
    if(communication.empty())
    {
        throw QString("激光器通讯实体为空");
    }
    _sendReply=dynamic_cast<BaseSendReply*>(communication.first());
}

void CNILaser::GetConnectStateCore(DeviceStateItems &items)
{
    QJsonObject obj;
    GetStatus(obj);
    items[0].Data=QString::number(obj["放电次数"].toInt());
}
