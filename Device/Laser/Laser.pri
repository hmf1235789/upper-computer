INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/DialogLaserControl.h \
    $$PWD/ethlasercmd.h \
    $$PWD/CNILaser.h \
    $$PWD/BaseLaser.h \
    $$PWD/AirCooledLaser.h

SOURCES += \
    $$PWD/DialogLaserControl.cpp \
    $$PWD/ethlasercmd.cpp \
    $$PWD/CNILaser.cpp \
    $$PWD/BaseLaser.cpp \
    $$PWD/AirCooledLaser.cpp

FORMS += \
    $$PWD/DialogLaserControl.ui
