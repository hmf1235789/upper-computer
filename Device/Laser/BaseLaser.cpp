﻿#include "BaseLaser.h"
#include "DialogLaserControl.h"

BaseLaser::BaseLaser(QObject *parent):BaseDevice(parent)
{

}

QWidget *BaseLaser::GetConfigWidget()
{
    DialogLaserControl *w=new DialogLaserControl();
    w->SetLaser(this);
    return w;
}

void BaseLaser::Open()
{

}

void BaseLaser::Close()
{

}

void BaseLaser::SetFlashlampSyncMode(LASER_SYNC_MODE mode)
{

}

void BaseLaser::SetFrequence(int freq)
{

}

void BaseLaser::SetEnergy(int level)
{

}

bool BaseLaser::GetStatus(QJsonObject &obj)
{
    obj={
        {"Enabled","OFF"},
        {"Energy",1000}
    };
    return true;
}

void BaseLaser::GetConnectStateCore(DeviceStateItems &items)
{
    items[0].Data="10000";
}

bool BaseLaser::Stop()
{
    Close();
    return true;
}

QList<int> BaseLaser::GetAdviceFrequency()
{
    QList<int> ret;
    if(_other.contains("AdviceFrequency"))
    {
        QJsonArray arr=_other["AdviceFrequency"].toArray();
        for(int i=0;i<arr.count();i++)
        {
            ret.append(arr[i].toInt());
        }
    }
    return ret;
}
