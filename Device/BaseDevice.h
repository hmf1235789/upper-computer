﻿#pragma execution_character_set("utf-8")
#ifndef IBASEDEVICEV2_H
#define IBASEDEVICEV2_H

#include <QObject>
#include <QWidget>
#include <QJsonObject>
#include <QSettings>

#include "BaseCommunication.h"
#include "BaseItem.h"
#include "CommunicationManager.h"
#include "DeviceStateItem.h"
#include "DeviceStateItems.h"

class BaseDevice : public BaseItem
{
    Q_OBJECT
public:
    Q_INVOKABLE BaseDevice(QObject *parent=nullptr);

    virtual void SetCommunication(QList<BaseCommunication*> communication);

    //在流程中止时，先停止，然后再复位
    virtual bool Stop();

    virtual bool Reset();

    virtual bool IsConnected();

public slots:
    DeviceStateItems GetDeviceStatusConfig();

    DeviceStateItems GetDeviceStateItems();

protected:
    virtual void GetConnectStateCore(DeviceStateItems &items);

protected:
    QList<BaseCommunication*> _communications;

    // BaseItem interface
public:
    virtual void SetConfig(QJsonObject obj);

    virtual QWidget *GetConfigWidget() override;

private:
    DeviceStateItems _items;
};

#endif // IBASEDEVICEV2_H
