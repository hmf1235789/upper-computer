// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: model_identify.proto

#include "model_identify.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// This is a temporary google only hack
#ifdef GOOGLE_PROTOBUF_ENFORCE_UNIQUENESS
#include "third_party/protobuf/version.h"
#endif
// @@protoc_insertion_point(includes)

class classfeaturedataDefaultTypeInternal {
 public:
  ::google::protobuf::internal::ExplicitlyConstructed<classfeaturedata>
      _instance;
} _classfeaturedata_default_instance_;
namespace protobuf_model_5fidentify_2eproto {
static void InitDefaultsclassfeaturedata() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  {
    void* ptr = &::_classfeaturedata_default_instance_;
    new (ptr) ::classfeaturedata();
    ::google::protobuf::internal::OnShutdownDestroyMessage(ptr);
  }
  ::classfeaturedata::InitAsDefaultInstance();
}

::google::protobuf::internal::SCCInfo<0> scc_info_classfeaturedata =
    {{ATOMIC_VAR_INIT(::google::protobuf::internal::SCCInfoBase::kUninitialized), 0, InitDefaultsclassfeaturedata}, {}};

void InitDefaults() {
  ::google::protobuf::internal::InitSCC(&scc_info_classfeaturedata.base);
}

::google::protobuf::Metadata file_level_metadata[1];

const ::google::protobuf::uint32 TableStruct::offsets[] GOOGLE_PROTOBUF_ATTRIBUTE_SECTION_VARIABLE(protodesc_cold) = {
  ~0u,  // no _has_bits_
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(::classfeaturedata, _internal_metadata_),
  ~0u,  // no _extensions_
  ~0u,  // no _oneof_case_
  ~0u,  // no _weak_field_map_
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(::classfeaturedata, classfeature_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(::classfeaturedata, classparameter_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(::classfeaturedata, names_),
};
static const ::google::protobuf::internal::MigrationSchema schemas[] GOOGLE_PROTOBUF_ATTRIBUTE_SECTION_VARIABLE(protodesc_cold) = {
  { 0, -1, sizeof(::classfeaturedata)},
};

static ::google::protobuf::Message const * const file_default_instances[] = {
  reinterpret_cast<const ::google::protobuf::Message*>(&::_classfeaturedata_default_instance_),
};

void protobuf_AssignDescriptors() {
  AddDescriptors();
  AssignDescriptors(
      "model_identify.proto", schemas, file_default_instances, TableStruct::offsets,
      file_level_metadata, NULL, NULL);
}

void protobuf_AssignDescriptorsOnce() {
  static ::google::protobuf::internal::once_flag once;
  ::google::protobuf::internal::call_once(once, protobuf_AssignDescriptors);
}

void protobuf_RegisterTypes(const ::std::string&) GOOGLE_PROTOBUF_ATTRIBUTE_COLD;
void protobuf_RegisterTypes(const ::std::string&) {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::internal::RegisterAllTypes(file_level_metadata, 1);
}

void AddDescriptorsImpl() {
  InitDefaults();
  static const char descriptor[] GOOGLE_PROTOBUF_ATTRIBUTE_SECTION_VARIABLE(protodesc_cold) = {
      "\n\024model_identify.proto\"O\n\020classfeatureda"
      "ta\022\024\n\014classfeature\030\001 \003(\001\022\026\n\016classparamet"
      "er\030\002 \003(\001\022\r\n\005names\030\003 \001(\tb\006proto3"
  };
  ::google::protobuf::DescriptorPool::InternalAddGeneratedFile(
      descriptor, 111);
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedFile(
    "model_identify.proto", &protobuf_RegisterTypes);
}

void AddDescriptors() {
  static ::google::protobuf::internal::once_flag once;
  ::google::protobuf::internal::call_once(once, AddDescriptorsImpl);
}
// Force AddDescriptors() to be called at dynamic initialization time.
struct StaticDescriptorInitializer {
  StaticDescriptorInitializer() {
    AddDescriptors();
  }
} static_descriptor_initializer;
}  // namespace protobuf_model_5fidentify_2eproto

// ===================================================================

void classfeaturedata::InitAsDefaultInstance() {
}
#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int classfeaturedata::kClassfeatureFieldNumber;
const int classfeaturedata::kClassparameterFieldNumber;
const int classfeaturedata::kNamesFieldNumber;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

classfeaturedata::classfeaturedata()
  : ::google::protobuf::Message(), _internal_metadata_(NULL) {
  ::google::protobuf::internal::InitSCC(
      &protobuf_model_5fidentify_2eproto::scc_info_classfeaturedata.base);
  SharedCtor();
  // @@protoc_insertion_point(constructor:classfeaturedata)
}
classfeaturedata::classfeaturedata(const classfeaturedata& from)
  : ::google::protobuf::Message(),
      _internal_metadata_(NULL),
      classfeature_(from.classfeature_),
      classparameter_(from.classparameter_) {
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  names_.UnsafeSetDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  if (from.names().size() > 0) {
    names_.AssignWithDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), from.names_);
  }
  // @@protoc_insertion_point(copy_constructor:classfeaturedata)
}

void classfeaturedata::SharedCtor() {
  names_.UnsafeSetDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

classfeaturedata::~classfeaturedata() {
  // @@protoc_insertion_point(destructor:classfeaturedata)
  SharedDtor();
}

void classfeaturedata::SharedDtor() {
  names_.DestroyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

void classfeaturedata::SetCachedSize(int size) const {
  _cached_size_.Set(size);
}
const ::google::protobuf::Descriptor* classfeaturedata::descriptor() {
  ::protobuf_model_5fidentify_2eproto::protobuf_AssignDescriptorsOnce();
  return ::protobuf_model_5fidentify_2eproto::file_level_metadata[kIndexInFileMessages].descriptor;
}

const classfeaturedata& classfeaturedata::default_instance() {
  ::google::protobuf::internal::InitSCC(&protobuf_model_5fidentify_2eproto::scc_info_classfeaturedata.base);
  return *internal_default_instance();
}


void classfeaturedata::Clear() {
// @@protoc_insertion_point(message_clear_start:classfeaturedata)
  ::google::protobuf::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  classfeature_.Clear();
  classparameter_.Clear();
  names_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  _internal_metadata_.Clear();
}

bool classfeaturedata::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  // @@protoc_insertion_point(parse_start:classfeaturedata)
  for (;;) {
    ::std::pair<::google::protobuf::uint32, bool> p = input->ReadTagWithCutoffNoLastTag(127u);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // repeated double classfeature = 1;
      case 1: {
        if (static_cast< ::google::protobuf::uint8>(tag) ==
            static_cast< ::google::protobuf::uint8>(10u /* 10 & 0xFF */)) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPackedPrimitive<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 input, this->mutable_classfeature())));
        } else if (
            static_cast< ::google::protobuf::uint8>(tag) ==
            static_cast< ::google::protobuf::uint8>(9u /* 9 & 0xFF */)) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadRepeatedPrimitiveNoInline<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 1, 10u, input, this->mutable_classfeature())));
        } else {
          goto handle_unusual;
        }
        break;
      }

      // repeated double classparameter = 2;
      case 2: {
        if (static_cast< ::google::protobuf::uint8>(tag) ==
            static_cast< ::google::protobuf::uint8>(18u /* 18 & 0xFF */)) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPackedPrimitive<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 input, this->mutable_classparameter())));
        } else if (
            static_cast< ::google::protobuf::uint8>(tag) ==
            static_cast< ::google::protobuf::uint8>(17u /* 17 & 0xFF */)) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadRepeatedPrimitiveNoInline<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 1, 18u, input, this->mutable_classparameter())));
        } else {
          goto handle_unusual;
        }
        break;
      }

      // string names = 3;
      case 3: {
        if (static_cast< ::google::protobuf::uint8>(tag) ==
            static_cast< ::google::protobuf::uint8>(26u /* 26 & 0xFF */)) {
          DO_(::google::protobuf::internal::WireFormatLite::ReadString(
                input, this->mutable_names()));
          DO_(::google::protobuf::internal::WireFormatLite::VerifyUtf8String(
            this->names().data(), static_cast<int>(this->names().length()),
            ::google::protobuf::internal::WireFormatLite::PARSE,
            "classfeaturedata.names"));
        } else {
          goto handle_unusual;
        }
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0) {
          goto success;
        }
        DO_(::google::protobuf::internal::WireFormat::SkipField(
              input, tag, _internal_metadata_.mutable_unknown_fields()));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:classfeaturedata)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:classfeaturedata)
  return false;
#undef DO_
}

void classfeaturedata::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:classfeaturedata)
  ::google::protobuf::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  // repeated double classfeature = 1;
  if (this->classfeature_size() > 0) {
    ::google::protobuf::internal::WireFormatLite::WriteTag(1, ::google::protobuf::internal::WireFormatLite::WIRETYPE_LENGTH_DELIMITED, output);
    output->WriteVarint32(static_cast< ::google::protobuf::uint32>(
        _classfeature_cached_byte_size_));
    ::google::protobuf::internal::WireFormatLite::WriteDoubleArray(
      this->classfeature().data(), this->classfeature_size(), output);
  }

  // repeated double classparameter = 2;
  if (this->classparameter_size() > 0) {
    ::google::protobuf::internal::WireFormatLite::WriteTag(2, ::google::protobuf::internal::WireFormatLite::WIRETYPE_LENGTH_DELIMITED, output);
    output->WriteVarint32(static_cast< ::google::protobuf::uint32>(
        _classparameter_cached_byte_size_));
    ::google::protobuf::internal::WireFormatLite::WriteDoubleArray(
      this->classparameter().data(), this->classparameter_size(), output);
  }

  // string names = 3;
  if (this->names().size() > 0) {
    ::google::protobuf::internal::WireFormatLite::VerifyUtf8String(
      this->names().data(), static_cast<int>(this->names().length()),
      ::google::protobuf::internal::WireFormatLite::SERIALIZE,
      "classfeaturedata.names");
    ::google::protobuf::internal::WireFormatLite::WriteStringMaybeAliased(
      3, this->names(), output);
  }

  if ((_internal_metadata_.have_unknown_fields() &&  ::google::protobuf::internal::GetProto3PreserveUnknownsDefault())) {
    ::google::protobuf::internal::WireFormat::SerializeUnknownFields(
        (::google::protobuf::internal::GetProto3PreserveUnknownsDefault()   ? _internal_metadata_.unknown_fields()   : _internal_metadata_.default_instance()), output);
  }
  // @@protoc_insertion_point(serialize_end:classfeaturedata)
}

::google::protobuf::uint8* classfeaturedata::InternalSerializeWithCachedSizesToArray(
    bool deterministic, ::google::protobuf::uint8* target) const {
  (void)deterministic; // Unused
  // @@protoc_insertion_point(serialize_to_array_start:classfeaturedata)
  ::google::protobuf::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  // repeated double classfeature = 1;
  if (this->classfeature_size() > 0) {
    target = ::google::protobuf::internal::WireFormatLite::WriteTagToArray(
      1,
      ::google::protobuf::internal::WireFormatLite::WIRETYPE_LENGTH_DELIMITED,
      target);
    target = ::google::protobuf::io::CodedOutputStream::WriteVarint32ToArray(
        static_cast< ::google::protobuf::int32>(
            _classfeature_cached_byte_size_), target);
    target = ::google::protobuf::internal::WireFormatLite::
      WriteDoubleNoTagToArray(this->classfeature_, target);
  }

  // repeated double classparameter = 2;
  if (this->classparameter_size() > 0) {
    target = ::google::protobuf::internal::WireFormatLite::WriteTagToArray(
      2,
      ::google::protobuf::internal::WireFormatLite::WIRETYPE_LENGTH_DELIMITED,
      target);
    target = ::google::protobuf::io::CodedOutputStream::WriteVarint32ToArray(
        static_cast< ::google::protobuf::int32>(
            _classparameter_cached_byte_size_), target);
    target = ::google::protobuf::internal::WireFormatLite::
      WriteDoubleNoTagToArray(this->classparameter_, target);
  }

  // string names = 3;
  if (this->names().size() > 0) {
    ::google::protobuf::internal::WireFormatLite::VerifyUtf8String(
      this->names().data(), static_cast<int>(this->names().length()),
      ::google::protobuf::internal::WireFormatLite::SERIALIZE,
      "classfeaturedata.names");
    target =
      ::google::protobuf::internal::WireFormatLite::WriteStringToArray(
        3, this->names(), target);
  }

  if ((_internal_metadata_.have_unknown_fields() &&  ::google::protobuf::internal::GetProto3PreserveUnknownsDefault())) {
    target = ::google::protobuf::internal::WireFormat::SerializeUnknownFieldsToArray(
        (::google::protobuf::internal::GetProto3PreserveUnknownsDefault()   ? _internal_metadata_.unknown_fields()   : _internal_metadata_.default_instance()), target);
  }
  // @@protoc_insertion_point(serialize_to_array_end:classfeaturedata)
  return target;
}

size_t classfeaturedata::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:classfeaturedata)
  size_t total_size = 0;

  if ((_internal_metadata_.have_unknown_fields() &&  ::google::protobuf::internal::GetProto3PreserveUnknownsDefault())) {
    total_size +=
      ::google::protobuf::internal::WireFormat::ComputeUnknownFieldsSize(
        (::google::protobuf::internal::GetProto3PreserveUnknownsDefault()   ? _internal_metadata_.unknown_fields()   : _internal_metadata_.default_instance()));
  }
  // repeated double classfeature = 1;
  {
    unsigned int count = static_cast<unsigned int>(this->classfeature_size());
    size_t data_size = 8UL * count;
    if (data_size > 0) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(
            static_cast< ::google::protobuf::int32>(data_size));
    }
    int cached_size = ::google::protobuf::internal::ToCachedSize(data_size);
    GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
    _classfeature_cached_byte_size_ = cached_size;
    GOOGLE_SAFE_CONCURRENT_WRITES_END();
    total_size += data_size;
  }

  // repeated double classparameter = 2;
  {
    unsigned int count = static_cast<unsigned int>(this->classparameter_size());
    size_t data_size = 8UL * count;
    if (data_size > 0) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(
            static_cast< ::google::protobuf::int32>(data_size));
    }
    int cached_size = ::google::protobuf::internal::ToCachedSize(data_size);
    GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
    _classparameter_cached_byte_size_ = cached_size;
    GOOGLE_SAFE_CONCURRENT_WRITES_END();
    total_size += data_size;
  }

  // string names = 3;
  if (this->names().size() > 0) {
    total_size += 1 +
      ::google::protobuf::internal::WireFormatLite::StringSize(
        this->names());
  }

  int cached_size = ::google::protobuf::internal::ToCachedSize(total_size);
  SetCachedSize(cached_size);
  return total_size;
}

void classfeaturedata::MergeFrom(const ::google::protobuf::Message& from) {
// @@protoc_insertion_point(generalized_merge_from_start:classfeaturedata)
  GOOGLE_DCHECK_NE(&from, this);
  const classfeaturedata* source =
      ::google::protobuf::internal::DynamicCastToGenerated<const classfeaturedata>(
          &from);
  if (source == NULL) {
  // @@protoc_insertion_point(generalized_merge_from_cast_fail:classfeaturedata)
    ::google::protobuf::internal::ReflectionOps::Merge(from, this);
  } else {
  // @@protoc_insertion_point(generalized_merge_from_cast_success:classfeaturedata)
    MergeFrom(*source);
  }
}

void classfeaturedata::MergeFrom(const classfeaturedata& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:classfeaturedata)
  GOOGLE_DCHECK_NE(&from, this);
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  ::google::protobuf::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  classfeature_.MergeFrom(from.classfeature_);
  classparameter_.MergeFrom(from.classparameter_);
  if (from.names().size() > 0) {

    names_.AssignWithDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), from.names_);
  }
}

void classfeaturedata::CopyFrom(const ::google::protobuf::Message& from) {
// @@protoc_insertion_point(generalized_copy_from_start:classfeaturedata)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void classfeaturedata::CopyFrom(const classfeaturedata& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:classfeaturedata)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool classfeaturedata::IsInitialized() const {
  return true;
}

void classfeaturedata::Swap(classfeaturedata* other) {
  if (other == this) return;
  InternalSwap(other);
}
void classfeaturedata::InternalSwap(classfeaturedata* other) {
  using std::swap;
  classfeature_.InternalSwap(&other->classfeature_);
  classparameter_.InternalSwap(&other->classparameter_);
  names_.Swap(&other->names_, &::google::protobuf::internal::GetEmptyStringAlreadyInited(),
    GetArenaNoVirtual());
  _internal_metadata_.Swap(&other->_internal_metadata_);
}

::google::protobuf::Metadata classfeaturedata::GetMetadata() const {
  protobuf_model_5fidentify_2eproto::protobuf_AssignDescriptorsOnce();
  return ::protobuf_model_5fidentify_2eproto::file_level_metadata[kIndexInFileMessages];
}


// @@protoc_insertion_point(namespace_scope)
namespace google {
namespace protobuf {
template<> GOOGLE_PROTOBUF_ATTRIBUTE_NOINLINE ::classfeaturedata* Arena::CreateMaybeMessage< ::classfeaturedata >(Arena* arena) {
  return Arena::CreateInternal< ::classfeaturedata >(arena);
}
}  // namespace protobuf
}  // namespace google

// @@protoc_insertion_point(global_scope)
