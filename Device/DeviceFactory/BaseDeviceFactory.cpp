﻿#include "BaseDeviceFactory.h"
#include "CNILaser.h"
#include "BaseIOModule.h"
#include "BaseDiastimeter.h"
#include "BaseTemperature.h"
#include "CL2CRSDriveControl.h"
#include "KHIOModuleDevice.h"
#include "KHKJPT100Sensor.h"
#include "TB5000Diastimeter.h"
#include "SdwDiastimeterDevice.h"
#include "BasePretreatment.h"
#include "PowderPretreatment.h"
#include "AirCooledLaser.h"
#include "GHDIOModuleDevice.h"

BaseDeviceFactory::BaseDeviceFactory()
{
    Register<BaseMotorController>();
    Register<CL2CRSDriveControl>();
    Register<CNILaser>();
    Register<BaseLaser>();
    Register<BaseIOModule>();
    Register<KHIOModuleDevice>();
    Register<GHDIOModuleDevice>();
    Register<BaseDiastimeter>();
    Register<BaseTemperature>();
    Register<KHKJPT100Sensor>();
    Register<SdwDiastimeterDevice>();
    Register<TB5000Diastimeter>();
    Register<BasePretreatment>();
    Register<PowderPretreatment>();
    Register<AirCooledLaser>();
}

BaseItem *BaseDeviceFactory::CreateInstance(QString concreteType)
{
//    qDebug()<<__FUNCTION__<<__LINE__<<concreteType;
    return BaseFactory::CreateInstance(concreteType);
}
