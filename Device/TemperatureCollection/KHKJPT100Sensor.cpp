﻿#include "KHKJPT100Sensor.h"
#include <QStringList>
#include <QDebug>


KHKJPT100Sensor::KHKJPT100Sensor(QObject *parent):BaseTemperature(parent)
{

}

KHKJPT100Sensor::~KHKJPT100Sensor()
{
    m_clocker.stop();
}

void KHKJPT100Sensor::getSensorValueSlot()
{
    if(_modbus->IsConnected())
    {
        QList<uint16_t> retList;
        _modbus->ReadOutRegisters(0x20,3,retList);
        _values.clear();
        for(auto item:retList)
        {
            _values.append(item/10.0);
        }
    }
}

void KHKJPT100Sensor::SetCommunication(QList<BaseCommunication *> communication)
{
    BaseDevice::SetCommunication(communication);
    if(communication.empty())
    {
        throw QString("KHKJPT100Sensor 未配置通讯实体！");
    }
    _modbus=dynamic_cast<BaseModbus*>(communication.first());
    connect(&m_clocker, &QTimer::timeout,this, &KHKJPT100Sensor::getSensorValueSlot);
//    m_clocker.setInterval(2000);
//    m_clocker.start(1000);
}

int KHKJPT100Sensor::GetTemperature(QList<double> &values)
{
    values=_values;
    return values.count();
}

void KHKJPT100Sensor::GetConnectStateCore(DeviceStateItems &items)
{
    getSensorValueSlot();
    if(items.count()==_values.count())
    {
        for(int i=0;i<items.count();i++)
        {
            items[i].Data=QString::number(_values.at(i));
        }
    }
}


