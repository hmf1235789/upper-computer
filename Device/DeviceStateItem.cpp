﻿#include "DeviceStateItem.h"
#include <QMap>
#include <QVariant>

DeviceStateItem::DeviceStateItem()
{
    Name="";
    Address="";
    IsConnected=false;
}

DeviceStateItem::DeviceStateItem(QString name, QString address)
{
    Name=name;
    Address=address;
    IsConnected=false;
}


QStringList DeviceStateItem::GetHeaders()
{
    QStringList ret;
    ret<<"设备名"<<"设备地址"<<"设备信息"<<"报警";
    return ret;
}

QMap<QString, QVariant> DeviceStateItem::GetMap()
{
    QMap<QString,QVariant> ret={
        {"设备名",Name},
        {"设备地址",Address},
        {"设备信息",Data},
        {"报警",Error}
    };
    return ret;
}
