﻿#ifndef DIALOGPRETREAMENT_H
#define DIALOGPRETREAMENT_H

#include <QDialog>
#include "BasePretreatment.h"

namespace Ui {
class DialogPretreament;
}
class BasePretreatment;

class DialogPretreament : public QDialog
{
    Q_OBJECT

public:
    explicit DialogPretreament(QWidget *parent = 0);

    ~DialogPretreament();

    void Init(BasePretreatment *pretreament);

private slots:
    void on_btnStart_clicked();

    void on_btnStop_clicked();

    void on_btnRestStatus_clicked();

    void on_btnGetStatus_clicked();

private:
    Ui::DialogPretreament *ui;
    BasePretreatment *_pretreatment=nullptr;
};

#endif // DIALOGPRETREAMENT_H
