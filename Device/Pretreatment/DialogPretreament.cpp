﻿#include "DialogPretreament.h"
#include "ui_DialogPretreament.h"

DialogPretreament::DialogPretreament(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogPretreament)
{
    ui->setupUi(this);
}

DialogPretreament::~DialogPretreament()
{
    delete ui;
}

void DialogPretreament::Init(BasePretreatment *pretreament)
{
    _pretreatment=pretreament;
}

void DialogPretreament::on_btnStart_clicked()
{
    _pretreatment->Start();
}

void DialogPretreament::on_btnStop_clicked()
{
    _pretreatment->Stop();
}

void DialogPretreament::on_btnRestStatus_clicked()
{
    _pretreatment->Reset();
}

void DialogPretreament::on_btnGetStatus_clicked()
{
   DeviceStateItems items=_pretreatment->GetDeviceStateItems();
   if(items.count()==2)
   {
       ui->txtLog->append(items[0].Data);
   }
}
