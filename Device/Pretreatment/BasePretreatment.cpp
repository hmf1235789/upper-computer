﻿#include "BasePretreatment.h"


BasePretreatment::BasePretreatment(QObject *parent):BaseDevice(parent)
{

}

void BasePretreatment::Start()
{
    qDebug()<<"模拟前处理设备启动";
}

bool BasePretreatment::Stop()
{
    qDebug()<<"模拟前处理设备停止";
    return true;
}

bool BasePretreatment::Reset()
{
    return true;
}

QWidget *BasePretreatment::GetConfigWidget()
{
    DialogPretreament *ret=new DialogPretreament();
    ret->Init(this);
    return ret;
}

void BasePretreatment::GetConnectStateCore(DeviceStateItems &items)
{

}
