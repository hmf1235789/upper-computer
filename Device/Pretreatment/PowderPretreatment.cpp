﻿#include "PowderPretreatment.h"

PowderPretreatment::PowderPretreatment(QObject *parent) : BasePretreatment(parent)
{

}

void PowderPretreatment::SetCommunication(QList<BaseCommunication *> communication)
{
    BasePretreatment::SetCommunication(communication);
    if(communication.count()!=1)
    {
        throw QString("%1未配置通讯实体!").arg(_name);
    }
    _modbus=dynamic_cast<BaseModbus*>(communication[0]);
    if(_modbus==nullptr)
    {
        throw QString("%1转型modbus失败!").arg(_name);
    }
}

bool PowderPretreatment::Stop()
{
    RunMode mode=RunMode::Off;
    QList<uint16_t> inputList;
    inputList<<mode;
    _modbus->WriteRegisters(0,1,inputList);
    return true;
}

void PowderPretreatment::Start()
{
    RunMode mode=RunMode::Single;
    QList<uint16_t> inputList;
    inputList<<mode;
    _modbus->WriteRegisters(0,1,inputList);
}

bool PowderPretreatment::Reset()
{
    QList<uint16_t> inputList;
    inputList<<RunMode::Off;
    return _modbus->WriteRegisters(0,1,inputList);
}

void PowderPretreatment::GetConnectStateCore(DeviceStateItems &items)
{
    QMap<RunMode,QString> map={
        {RunMode::Off,"系统关闭"},
        {RunMode::Single,"正在运行"},
        {RunMode::Ready,"物料就位"}
    };
    if(items.count()==2)
    {
        uint16_t val;
        _modbus->ReadInRegister(0,val);
        RunMode mode=static_cast<RunMode>(val);
        items[0].Data=map.contains(mode)?map[mode]:"未知状态";

        QList<uint16_t> ret;
        _modbus->ReadOutRegisters(21,2,ret);
        int retStstus=(ret.at(0)*65535+ret.at(1));
        double dval=((double)retStstus)/1000;
        items[1].Data=QString::number(dval);
    }
}
