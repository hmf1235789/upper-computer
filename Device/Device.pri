INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/BaseDevice.h \
    $$PWD/buttondelegate.h \
    $$PWD/DeviceManager.h \
    $$PWD/DeviceHelper.h \
    $$PWD/DeviceStateItem.h \
    $$PWD/DeviceStateItems.h

SOURCES += \
    $$PWD/buttondelegate.cpp \
    $$PWD/BaseDevice.cpp \
    $$PWD/DeviceManager.cpp \
    $$PWD/DeviceHelper.cpp \
    $$PWD/DeviceStateItem.cpp \
    $$PWD/DeviceStateItems.cpp



include("./IOModuleDevice/IOModuleDevice.pri")
include("./MovePlatFormController/MovePlatFormController.pri")
include("./DeviceFactory/DeviceFactory.pri")
include("./Laser/Laser.pri")
include("./Diastimeter/Diastimeter.pri")
include("./TemperatureCollection/TemperatureCollection.pri")
include("./Pretreatment/Pretreatment.pri")
