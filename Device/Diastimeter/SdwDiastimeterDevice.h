﻿#ifndef SDWDIASTIMETERDEVICE_H
#define SDWDIASTIMETERDEVICE_H

#include <QObject>
#include "BaseDevice.h"
#include "BaseModbus.h"
#include "BaseDiastimeter.h"

class SdwDiastimeterDevice :public BaseDiastimeter
{
    Q_OBJECT
public:
    Q_INVOKABLE SdwDiastimeterDevice(QObject *parent = nullptr);

    bool SetMeasureMode(EOpenModeTag measureMode);

    bool GetDiastimeterValue(double &value);
signals:

public slots:

private:
    BaseModbus* _modbus=nullptr;

    // BaseDevice interface
public:
    void SetCommunication(QList<BaseCommunication *> communication) override;
};

#endif // SDWDIASTIMETERDEVICE_H
