﻿#include "BaseDiastimeter.h"
#include "dialogdiastimeter.h"

BaseDiastimeter::BaseDiastimeter(QObject *parent):BaseDevice(parent)
{

}

bool BaseDiastimeter::SetMeasureMode(EOpenModeTag measureMode)
{
    return true;
}

bool BaseDiastimeter::GetDiastimeterValue(double &value)
{
    value=_value;
    return true;
}

bool BaseDiastimeter::SetDiastimeterValue(double value)
{
    _value=value;
    return true;
}

QWidget *BaseDiastimeter::GetConfigWidget()
{
    DialogDiastimeter *w=new DialogDiastimeter();
    w->SetDiastimeter(this);
    return w;
}

void BaseDiastimeter::GetConnectStateCore(DeviceStateItems &items)
{
    items[0].Data=QString::number(_value);
}
