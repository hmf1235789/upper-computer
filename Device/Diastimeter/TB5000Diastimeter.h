﻿#ifndef TB5000DIASTIMETER_H
#define TB5000DIASTIMETER_H

#include <QObject>

#include "BaseDevice.h"
#include "BaseModbus.h"
#include "BaseDiastimeter.h"

class TB5000Diastimeter : public BaseDiastimeter
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit TB5000Diastimeter(QObject *parent = nullptr);

signals:

public slots:
private:
    BaseModbus* _modbus=nullptr;

    // BaseDiastimeter interface
public:
    virtual bool GetDiastimeterValue(double &value) override;

    // BaseDevice interface
public:
    void SetCommunication(QList<BaseCommunication *> communication) override;

    // BaseDevice interface
protected:
    virtual void GetConnectStateCore(DeviceStateItems &items) override final;
};

#endif // TB5000DIASTIMETER_H
