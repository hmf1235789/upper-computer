﻿#ifndef DIALOGDIASTIMETER_H
#define DIALOGDIASTIMETER_H

#include <QDialog>
#include "SdwDiastimeterDevice.h"
#include "BaseDiastimeter.h"

namespace Ui {
class DialogDiastimeter;
}

class DialogDiastimeter : public QDialog
{
    Q_OBJECT

public:
    explicit DialogDiastimeter(QWidget *parent = 0);
    ~DialogDiastimeter();
    void SetDiastimeter(BaseDiastimeter *diastimeter);

private slots:
    void on_buttonBox_accepted();

    void on_btnGetVal_clicked();

    void on_btnSetMode_clicked();

    void on_lineEdit_returnPressed();

private:
    Ui::DialogDiastimeter *ui;
    BaseDiastimeter *_diastimeter=nullptr;
};

#endif // DIALOGDIASTIMETER_H
