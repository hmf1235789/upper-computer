﻿#pragma execution_character_set("utf-8")
#include <QMetaEnum>
#include <QMessageBox>

#include "dialogdiastimeter.h"
#include "ui_dialogdiastimeter.h"

#include "DeviceManager.h"

QList<EOpenModeTag> modes={EOpenModeTag::eMeasMode_Min,EOpenModeTag::eMeasMode_Close,eMeasMode_Single,
                 eMeasMode_Continuous,eMeasMode_DelayAutoMeas,eMeasMode_Max};
QList<QString> modeStrs={"Min","Close","Single",
                        "Continuous","DelayAutoMeasure","Max"};

DialogDiastimeter::DialogDiastimeter(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogDiastimeter)
{
    ui->setupUi(this);
    ui->cmbRunMode->clear();
//    QMetaEnum metaEnum = QMetaEnum::fromType<EOpenMode>();
    for (int i = 0; i <modes.count(); i++)
    {
        //metaEnum.key(i) 是枚举类型 每一个枚举成员的名称；metaEnum.value(i)是每一个枚举成员后面对应的值
        ui->cmbRunMode->addItem(modeStrs.at(i),modes.at(i));
    }
}

DialogDiastimeter::~DialogDiastimeter()
{
    delete ui;
}

void DialogDiastimeter::SetDiastimeter(BaseDiastimeter *diastimeter)
{
    _diastimeter=diastimeter;
}

void DialogDiastimeter::on_buttonBox_accepted()
{
//    _sdwDevice->SetMeasureMode(selectMode)
}

void DialogDiastimeter::on_btnGetVal_clicked()
{
    double retVal=-1;
    _diastimeter->GetDiastimeterValue(retVal);
    ui->lineEdit->setText(QString("%1").arg(retVal));
}

void DialogDiastimeter::on_btnSetMode_clicked()
{
    if(ui->cmbRunMode->currentIndex()!=-1)
    {
        EOpenModeTag selectMode=modes.at(ui->cmbRunMode->currentIndex());
        bool setRet=_diastimeter->SetMeasureMode(selectMode);
        QMessageBox::information(this,"提示",setRet?"设置成功":"设置失败",QMessageBox::Ok);
    }
}

void DialogDiastimeter::on_lineEdit_returnPressed()
{
    _diastimeter->SetDiastimeterValue(ui->lineEdit->text().toDouble());
}
