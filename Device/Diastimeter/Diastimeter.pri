INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/dialogdiastimeter.h \
    $$PWD/BaseDiastimeter.h \
    $$PWD/TB5000Diastimeter.h \
    $$PWD/SdwDiastimeterDevice.h

SOURCES += \
    $$PWD/dialogdiastimeter.cpp \
    $$PWD/BaseDiastimeter.cpp \
    $$PWD/TB5000Diastimeter.cpp \
    $$PWD/SdwDiastimeterDevice.cpp

FORMS += \
    $$PWD/dialogdiastimeter.ui
