﻿#include <QDebug>
#include <QTimer>

#include "IOItem.h"
#include "TimerManager.h"

IOItem::IOItem(int addr, IODeviceType deviceType, BaseIOModule *ioModule):QObject(ioModule)
{
    _addr=addr;
    _deviceType=deviceType;
    _ioModule=ioModule;
}

void IOItem::SetName(QString name)
{
    _name=name;
}

QString IOItem::GetName()
{
    return _name;
}

int IOItem::Addr()
{
    return _addr;
}

QString IOItem::DeviceTypeString()
{
    return _deviceType==IODeviceType::Input?"Input":"Output";
}

IODeviceType IOItem::DeviceType()
{
    return _deviceType;
}

bool IOItem::GetStatus()
{
    return _status;
}

bool IOItem::SetStatus(bool status)
{
    _status=status;
    return true;
}

bool IOItem::SetStatusDelayReset(bool status,int delaySwitchSecond)
{
    bool ret=_ioModule->WriteStatus(_name,status);
    _status=status;
    qDebug()<<__FUNCTION__<<__LINE__<<QString("%1设置%2").arg(_name).arg(status);
    if(ret&&delaySwitchSecond>0)
    {
        QTimer* timer= TimerManager::Instance().CreatSingleSloteTimer(delaySwitchSecond*1000);
        timer->setSingleShot(true);
        timer->setInterval(delaySwitchSecond*1000);
        QObject::connect(timer,&QTimer::timeout,this,[=]{
            _ioModule->WriteStatus(_name,!status);
            _status=!status;
            qDebug()<<__FUNCTION__<<__LINE__<<QString("%1秒后%2设置%3").arg(delaySwitchSecond).arg(_name).arg(_status);
        });
        timer->start();
    }

    return true;
}

