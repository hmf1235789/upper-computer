﻿#include <QJsonArray>
#include <QDebug>

#include "BaseIOModule.h"
#include "FormIOModule.h"

BaseIOModule::BaseIOModule(QObject *parent):BaseDevice(parent)
{

}

QList<IOItem *> BaseIOModule::GetAllIOItems()
{
    return _allIOItems;
}

bool BaseIOModule::ReadStatus(QString ioName, bool &status)
{
    IOItem *item=GetIOItem(ioName);
    status=item->GetStatus();
    return true;
}

bool BaseIOModule::WriteStatus(QString ioName, bool status)
{
//    IOItem *item=GetIOItem(ioName);
//    item->SetStatus(status);
    return true;
}

IOItem* BaseIOModule::GetIOItem(QString ioName)
{
    for(auto item:_allIOItems)
    {
        if(item->GetName()==ioName)
        {
            return item;
        }
    }
    throw QString("未发现%1").arg(ioName);
}


QWidget *BaseIOModule::GetConfigWidget()
{
    FormIOModule *w=new FormIOModule();
    w->SetIOModule(this);
    return w;
}

void BaseIOModule::SetOtherConfig(QJsonObject other)
{
    BaseDevice::SetOtherConfig(other);
    QJsonArray arr=other["Items"].toArray();
    if(!other.contains("Input"))
    {
        throw QString("IOModule未配置Input");
    }
    QJsonArray arrInput=other["Input"].toArray();
    if(_communications.count()>=1)
    {
        for(auto iter:arr)
        {
            QJsonObject iterObj=iter.toObject();
//            IOItem *item=new IOItem(iterObj["addr"].toInt(),IODeviceType::Input,this);
//            item->SetName(iterObj["name"].toString());
            _showItems.append(iterObj["name"].toString());
        }

        for(auto iter:arrInput)
        {
            QJsonObject iterObj=iter.toObject();
            IOItem *item=new IOItem(iterObj["addr"].toInt(),IODeviceType::Input,this);
            item->SetName(iterObj["name"].toString());
            _allIOItems.append(item);
        }

        QJsonArray arrOutput=other["Output"].toArray();
        for(auto iter:arrOutput)
        {
            QJsonObject iterObj=iter.toObject();
            IOItem *item=new IOItem(iterObj["addr"].toInt(),IODeviceType::Output,this);
            item->SetName(iterObj["name"].toString());
            _allIOItems.append(item);
        }
        qSort(_allIOItems.begin(),_allIOItems.end(),[](IOItem* a,IOItem* b){return a->Addr()<b->Addr();});
    }
}

void BaseIOModule::GetConnectStateCore(DeviceStateItems &items)
{
    for(int i=0;i<items.count();i++)
    {
        bool status=false;
        ReadStatus(items[i].DataType,status);
        items[i].Data=QString::number(status);
    }
}

