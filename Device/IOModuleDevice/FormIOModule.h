﻿#ifndef FORMKHDEBUG_H
#define FORMKHDEBUG_H

#include <QWidget>
#include <QMainWindow>
#include <QStandardItemModel>
#include <QMessageBox>
#include <QTimer>
#include <QVBoxLayout>

#include "buttondelegate.h"
#include "BaseModbus.h"
#include "IOItem.h"
#include "BaseIOModule.h"

namespace Ui {
class FormIOModule;
}

class FormIOModule : public QDialog
{
    Q_OBJECT

public:
    explicit FormIOModule(QWidget *parent = 0);
    ~FormIOModule();
    void SetIOModule(BaseIOModule *ioModule);
    void Init();

private:
    Ui::FormIOModule *ui;
    QStandardItemModel *_model;
    QMyTableViewBtnDelegate *m_btnDelegate;
    QTimer *_timer;
    int _timerId;
    QList<IOItem*> _allIODevice;
    BaseIOModule *_ioModule=nullptr;

    QList<QLabel*> *_labelList=nullptr;
    QString GetColorStyleSheet(QString color);

public slots:
    void Open(const QModelIndex &index);
    void Close(const QModelIndex &index);
    void on_btnStartDetect_clicked();

    // QObject interface
protected:
    virtual void timerEvent(QTimerEvent *event) override;
};

#endif // FORMKHDEBUG_H
