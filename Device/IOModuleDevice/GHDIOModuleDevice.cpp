﻿#include "GHDIOModuleDevice.h"

GHDIOModuleDevice::GHDIOModuleDevice(QObject *parent) : KHIOModuleDevice(parent)
{

}

void GHDIOModuleDevice::GetConnectStateCore(DeviceStateItems &items)
{
    if(_inReading)
    {
        return;
    }

    _inReading=true;
    QList<bool> output;
    _modbus->ReadOutBits(512,8,output);
    int offSet=items.count();
    for(int i=0;i<output.count();i++)
    {
        bool status=output.at(i);
        _allIOItems.at(i+offSet)->SetStatus(!status);
    }
    _inReading=false;
}

bool GHDIOModuleDevice::WriteStatus(QString ioName, bool status)
{
    return KHIOModuleDevice::WriteStatus(ioName,!status);
}
