﻿#ifndef DEVICEMANAGER_H
#define DEVICEMANAGER_H
#include "Head.h"
#include <QJsonObject>
#include <QObject>
#include <QList>
#include <QThread>

#include "DeviceFactory/BaseDeviceFactory.h"
#include "BaseDevice.h"
#include "BaseManager.h"
#include "DeviceStateItems.h"

class DeviceManager:public BaseManager
{
    Q_OBJECT

private:
    DeviceManager();
    ~DeviceManager();
    DeviceManager(const DeviceManager&)=delete;
    DeviceManager& operator=(const DeviceManager&)=delete;

public:
    static DeviceManager& Instance();

    QList<BaseDevice*> GetDevices(QJsonArray names);

    void SetRecord(bool isRecord);

    void Stop();
    void Reset();

    void WaitForEnd();

    void WaitForMonitorPause();
    void StartMonitor();
    void SetMonitorInterval(int intervalSecond);

    DeviceStateItems GetDeviceStateItems();

    void SetEnableDeviceMonitor(bool enableMonitor);

signals:
    void StateUpdated(DeviceStateItems items);

private:
    bool _isRecord=false;
    bool _needMonitor=true;
    bool _isMonitoring=false;
    bool _enableMonitor=false;
    int _monitorDeviceId=0;
    DeviceStateItems _stateItems;

    void RecordItem(DeviceStateItems itemList);

    // QObject interface
protected:
    virtual void timerEvent(QTimerEvent *event) override;
};

#endif // DEVICEFACTORY_H
