﻿#include <QDebug>
#include "BaseDevice.h"

BaseDevice::BaseDevice(QObject *parent):BaseItem(parent)
{

}

void BaseDevice::SetCommunication(QList<BaseCommunication *> communication)
{
    _communications=communication;
}

void BaseDevice::SetConfig(QJsonObject obj)
{
    BaseItem::SetConfig(obj);
    CommunicationManager &manager=CommunicationManager::Instance();
    auto comm=manager.GetCommunications(obj["Communication"].toArray());
    SetCommunication(comm);
}

QWidget *BaseDevice::GetConfigWidget()
{
    static QWidget w;
    // 设置窗口标题
    w.setWindowTitle("未配置界面");
    return &w;
}

bool BaseDevice::Stop()
{
    return true;
}

bool BaseDevice::Reset()
{
    return true;
}

bool BaseDevice::IsConnected()
{
    return _communications.count()>0?_communications.at(0)->IsConnected():false;
}

DeviceStateItems BaseDevice::GetDeviceStatusConfig()
{
    if(!_obj["Monitor"].toBool())
    {
        return DeviceStateItems();
    }
    DeviceStateItems ret;
    QString friendName=_obj["FriendlyName"].toString();
    if(_other.contains("Items"))
    {
        DeviceStateItem item=_communications.empty()?DeviceStateItem(friendName,""):
                                                      DeviceStateItem(friendName,_communications.at(0)->GetCommunicationParam());
        for(QJsonValue jItem:_other["Items"].toArray())
        {
            item.DataType=jItem.toObject()["name"].toString();
            ret.append(item);
        }
    }
    return ret;
}

DeviceStateItems BaseDevice::GetDeviceStateItems()
{
    DeviceStateItems ret=GetDeviceStatusConfig();
    bool isConnected=IsConnected();
    if(!isConnected)
    {
        return ret;
    }
    if(ret.count()!=0)
    {
        for(int i=0;i<ret.count();i++)
        {
            ret[i].IsConnected=isConnected;
        }

        if(isConnected)
        {
            GetConnectStateCore(ret);
        }
        else
        {
            for(int i=0;i<ret.count();i++)
            {
                ret[i].Data="--";
            }
        }
    }
    _items.clear();
    _items.append(ret);
    return ret;
}

void BaseDevice::GetConnectStateCore(DeviceStateItems &items)
{

}

