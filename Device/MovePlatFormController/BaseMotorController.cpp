﻿#include "BaseMotorController.h"
#include "dialogmoveplatform.h"
#include "WaitUtils.h"

BaseMotorController::BaseMotorController(QObject *parent):BaseDevice(parent)
{
    Q_UNUSED(parent);
}

BaseMotorController::~BaseMotorController()
{
    _configW->deleteLater();
}

void BaseMotorController::SetCurrentAxisType(BaseMotorController::AxisType axisType)
{
    _axisType=axisType;
}

void BaseMotorController::SetMotorEnable(bool enable)
{

}

int BaseMotorController::SetVelocity(VEL_TYPE_ENUM eVelType, double dVelocity)
{
    qDebug()<<__FUNCTION__<<"eVelType="<<eVelType<<"SetVelocity="<<dVelocity;
    return 0;
}

bool BaseMotorController::Stop()
{
    qDebug()<<__FUNCTION__<<"stop seccessed!";
    return true;
}

bool BaseMotorController::SaveParam()
{
    return true;
}

bool BaseMotorController::Reset()
{
    return true;
}

QWidget *BaseMotorController::GetConfigWidget()
{
    DialogMovePlatform *w=new DialogMovePlatform();
    w->SetDevice(this);
    return w;
}

QSharedPointer<QWidget> BaseMotorController::GetConfigWidgetZZZ()
{
    QSharedPointer<DialogMovePlatform> w=QSharedPointer<DialogMovePlatform>(new DialogMovePlatform());
    w.data()->SetDevice(this);
    return w;
}

int BaseMotorController::RelativeMove(double dRelativeMove)
{
    Q_UNUSED(dRelativeMove);
    qDebug()<<__FUNCTION__<<"RelativeMove ="<<dRelativeMove;
    return 0;
}

int BaseMotorController::AbsoluteMove(double dMove)
{
    _motorStatus=true;
    WaitUtils::WaitMsNoException(2000);
    _motorStatus=false;
    return 0;
}

int BaseMotorController::SetCurrentPositionToZero()
{
    qDebug()<<__FUNCTION__<<"RtnToZero seccessed!";
    return 0;
}

int BaseMotorController::GetPosition(double &dPosition)
{
     qDebug()<<__FUNCTION__<<"GetPosition seccessed!";
     dPosition = 10;
     return 0;
}

void BaseMotorController::SetLimit(QList<MotorIOItem> items)
{

}

void BaseMotorController::ReturnToZero(BaseMotorController::DirectionType direction, LimitType limit)
{

}

void BaseMotorController::SetAxisDriection(BaseMotorController::AxisDirectionType axisType)
{

}

void BaseMotorController::SetMaxElectricity(int electricity)
{

}

QList<StateItem> BaseMotorController::GetStateItems()
{
    return QList<StateItem>();
}
