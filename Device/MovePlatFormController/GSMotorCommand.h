﻿#ifndef GSMOTORCOMMAND_H
#define GSMOTORCOMMAND_H
#include <QByteArray>

class GSMotorCommand
{
public:
    GSMotorCommand();

    QByteArray GetWriteByteArray();

private:
    unsigned char _header=0xfe;
    unsigned char _deviceNo=0x01;
    unsigned char _funcCode=0x08;
};

#endif // GSMOTORCOMMAND_H
