﻿#include "DialogControl.h"
#include "ui_DialogControl.h"
#include "WaitUtils.h"
#include "widgetutils.h"

DialogControl::DialogControl(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogControl)
{
    ui->setupUi(this);
}

DialogControl::~DialogControl()
{
    delete ui;
}

void DialogControl::SetController(BaseController *controller)
{
    _controller=controller;
    auto cmds=_controller->GetCommands();
    ui->cmbAllCmd->clear();
    for(int i=0;i<cmds.count();i++)
    {
        ui->cmbAllCmd->addItem(cmds[i]->GetFriendlyName());

        ui->cmbAllCmd->setItemData(i,cmds[i]->Name(),Qt::DecorationRole);
        ui->cmbAllCmd->setItemData(i,cmds[i]->GetFriendlyName(),Qt::DisplayRole);
    }
}

void DialogControl::on_btnRunSingle_clicked()
{
    if(ui->cmbAllCmd->currentIndex()==-1||_controller==nullptr)
    {
        return;
    }

    WidgetUtils::Execute(sender(),[this]()
    {
        WaitUtils::Reset();
        BaseCommand *cmd=_controller->GetCommand(ui->cmbAllCmd->currentData(Qt::DecorationRole).toString());
        cmd->Execute();
    },true);
}
