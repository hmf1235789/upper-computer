INCLUDEPATH += $$PWD


HEADERS += \
    $$PWD/ControllerManager.h \
    $$PWD/BaseController.h \
    $$PWD/DialogControl.h

SOURCES += \
    $$PWD/ControllerManager.cpp \
    $$PWD/BaseController.cpp \
    $$PWD/DialogControl.cpp

FORMS += \
    $$PWD/DialogControl.ui

include(./ControllerFactory/ControllerFactory.pri)
