﻿#include "ControllerManager.h"
#include "JsonUtils.h"
#include "DeviceManager.h"
#include "WidgetUtils.h"
//#include "NotificationManager.h"
//#include "GlobalVar.h"
//#include "BaseClietControlPanel.h"

ControllerManager::ControllerManager(QObject *parent) : BaseManager(parent)
{
    _factory=new BaseControllerFactory();
}

ControllerManager::~ControllerManager()
{

}

ControllerManager &ControllerManager::Instance()
{
    static ControllerManager _instance;
    return _instance;
}

BaseController *ControllerManager::GetController(QString name)
{
    return BaseManager::GetItem<BaseController>(name);
}

void ControllerManager::SetGetMeasureModeFunc(std::function<QString()> delegate)
{
    _delegate=delegate;
}

void ControllerManager::SetMeasureMode(QString measureMode)
{

}

void ControllerManager::OnPanelHandleString(QString mode_)
{
    auto mode=mode_=="startMeasure"?ControlTypeZZZ::StartMeasure:ControlTypeZZZ::StopMeasure;
    OnPanelHandle(mode);
}

void ControllerManager::OnPanelHandle(ControlTypeZZZ mode_)
{
    if(!_delegate)
    {
        throw QString("请设置测量模式回调函数!");
    }

    QString mode=_delegate();
    BaseController *_controller=GetController(mode);
    if(_controller==nullptr)
    {
        return;
    }

    if(_currentMode=="Measureing"&&mode_==ControlTypeZZZ::StartMeasure)
    {
        qDebug()<<__FUNCTION__<<__LINE__<<"收到控制信息，但不作处理!";
        return;
    }

    if(mode_==ControlTypeZZZ::StartMeasure)
    {
        ChangeStatus(RunModeZZZ::Measureing);
        //关闭设备监控
        SetMonitor(false);
        bool noErr=WidgetUtils::Execute(nullptr,[this,&_controller]{
             _controller->Execute();
        },false);

        if(!noErr)
        {
            //打开设备监控
            SetMonitor(true);
            _controller->Stop();
        }
        ChangeStatus(RunModeZZZ::Idle);
//        NotificationManager::Instance().OnReport("测量结束！",100);
    }
    else if(mode_==ControlTypeZZZ::StopMeasure)
    {
        ChangeStatus(RunModeZZZ::Stop);
        _controller->Stop();
        SetMonitor(true);
        ChangeStatus(RunModeZZZ::Idle);
    }
}

void ControllerManager::InitConfig()
{
    BaseManager::InitConfig();
}

void ControllerManager::SetMonitor(bool enable)
{
    if(enable)
    {
        DeviceManager::Instance().StartMonitor();
//        CommunicationManager::Instance().StartMonitor();
        qDebug()<<__FUNCTION__<<__LINE__<<"启用监控";
    }
    else
    {
        DeviceManager::Instance().WaitForMonitorPause();
//        CommunicationManager::Instance().WaitForMonitorPause();
        qDebug()<<__FUNCTION__<<__LINE__<<"暂停监控";
    }
}

void ControllerManager::ChangeStatus(RunModeZZZ mode)
{
//    QMap<BaseControlPanel::RunMode,QString> statusMap={
//        {BaseControlPanel::RunMode::Stop,"Stop"},
//        {BaseControlPanel::RunMode::Measureing,"Measureing"},
//        {BaseControlPanel::RunMode::Idle,"Idle"}
//    };
    _currentMode=mode;
    emit UpdateDeviceStatus(mode);
//    DeviceManager::Instance().SetRecord(mode==BaseControlPanel::RunMode::Measureing);
}



