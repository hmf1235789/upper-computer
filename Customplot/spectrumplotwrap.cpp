﻿#include <algorithm>
#include <QDebug>
#include "spectrumplotwrap.h"
#include "DialogCustomPlot.h"
#include "spectrumplotwrap.h"


SpectrumPlotWrap::SpectrumPlotWrap(QCustomPlot *plot, QSettings *set)
{
    _plot=plot;
    _set=set;
    _plot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes |
                                    QCP::iSelectLegend | QCP::iSelectPlottables);
    _plot->yAxis->setRange(-100,30000);

    //"波长[nm]", "强度"
    _plot->xAxis->setLabel("波长[nm]");
    _plot->yAxis->setLabel("强度");

    _title = new QCPTextElement(_plot, "光谱", QFont("sans", 12, QFont::Bold));
    _title->setVisible(true);
    _plot->plotLayout()->insertRow(0);
    _plot->plotLayout()->addElement(0,0,_title);
    connect(_plot,&QCustomPlot::mouseDoubleClick,this,&SpectrumPlotWrap::ShowInitDialog);

    _lockAxis=false;
    _upper=2000;
    GetPlotConfig(_lockAxis,_upper);

    connect(_plot, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mousePress()));
    connect(_plot, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(mouseWheel()));
}

void SpectrumPlotWrap::mousePress()
{
  // if an axis is selected, only allow the direction of that axis to be dragged
  // if no axis is selected, both directions may be dragged

  if (_plot->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
    _plot->axisRect()->setRangeDrag(_plot->xAxis->orientation());
  else if (_plot->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
    _plot->axisRect()->setRangeDrag(_plot->yAxis->orientation());
  else
    _plot->axisRect()->setRangeDrag(Qt::Horizontal|Qt::Vertical);
}

void SpectrumPlotWrap::mouseWheel()
{
  // if an axis is selected, only allow the direction of that axis to be zoomed
  // if no axis is selected, both directions may be zoomed

  if (_plot->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
    _plot->axisRect()->setRangeZoom(_plot->xAxis->orientation());
  else if (_plot->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
    _plot->axisRect()->setRangeZoom(_plot->yAxis->orientation());
  else
    _plot->axisRect()->setRangeZoom(Qt::Horizontal|Qt::Vertical);
}

void SpectrumPlotWrap::ShowInitDialog()
{
    DialogCustomPlot dialog(_lockAxis,_upper);
    dialog.exec();
    SetPlotConfig(_lockAxis,_upper);
}


void SpectrumPlotWrap::SetXYLabel(QString xLabel, QString yLabel)
{
    _plot->xAxis->setLabel(xLabel);
    _plot->yAxis->setLabel(yLabel);
}

void SpectrumPlotWrap::SetGraph(QStringList names)
{
    _plot->clearGraphs();
    for(int i=0;i<names.count();i++)
    {
        QPen pen;
        double period = 2 * M_PI;

        double red = qSin(i * 0.5 + 1.2) * 127 + 128;
        double green = qSin(i * 0.5 + 0.0) * 127 + 128;
        double blue = qSin(i * 0.3 + 1.5) * 127 + 128;

        pen.setColor(QColor(static_cast<int>(red), static_cast<int>(green), static_cast<int>(blue)));
//        pen.setColor(QColor(qSin(i*0.5+1.2)*160+80, qSin(i*0.3+0)*80+80, qSin(i*0.3+1.5)*80+80));
        pen.setWidthF(0.8);
        _plot->addGraph();
        _plot->graph()->setPen(pen); // 曲线的颜色
        _plot->graph()->setName(names.at(i));
    }
    _plot->legend->setVisible(true);
    _plot->axisRect()->insetLayout()->setInsetAlignment(0,Qt::AlignBottom|Qt::AlignRight);
}

void SpectrumPlotWrap::SetColors(QList<QColor> colors)
{
    for(int i=0;i<colors.count();i++)
    {
        QPen pen;
        pen.setColor(colors.at(i));
        pen.setWidthF(0.8);
        _plot->graph(i)->setPen(pen);
    }
}

int SpectrumPlotWrap::GraphCount()
{
    return _plot->graphCount();
}

void SpectrumPlotWrap::Plot(const QList<QPointF> &item,int index)
{
    QVector<double> x,y;
    x.clear();
    y.clear();
    foreach (auto p, item) {
        x.append(p.x());
        y.append(p.y());
    }
    _plot->graph(index)->setData(x,y,true);

    _plot->xAxis->rescale();
    if(_lockAxis)
    {
        _plot->yAxis->setRangeUpper(_upper);
    }
    else
    {
        _plot->yAxis->rescale();
    }
    _plot->replot();
}

void SpectrumPlotWrap::plotXY(const QList<double> &x, const QList<double> &y, int index, const QString &lineType)
{
    QCPGraph *graph = _plot->graph(index);
    if(graph==nullptr)
    {
        qDebug()<<__FUNCTION__<<__LINE__;
    }
    qDebug()<<__FUNCTION__<<__LINE__<<graph;
    if (lineType == "p") {
     graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 5));
     graph->setLineStyle(QCPGraph::lsNone);
    } else {
     graph->setLineStyle(QCPGraph::lsLine);
    }
    qDebug()<<__FUNCTION__<<__LINE__;

    graph->setData(x.toVector(), y.toVector());

    double maxy=*std::max_element(y.begin(), y.end());
    _plot->yAxis->setRange(-(maxy) * 0.1, (maxy) * 1.1);
    _plot->xAxis->rescale();
    _plot->replot();
 }

void SpectrumPlotWrap::Clear(int index)
{
    _plot->graph(index)->setData(QVector<double>(),QVector<double>());
    _plot->replot();
}

void SpectrumPlotWrap::Plot(const QList<QList<QPointF> > &items)
{
    for(int row=0;row<_plot->graphCount();row++)
    {
        Plot(items.at(row),row);
    }
}

void SpectrumPlotWrap::SetTitle(QString title)
{
    _title->setText(title);
    _plot->replot();
}

void SpectrumPlotWrap::SetPlotConfig(bool lockAxis,double upper)
{
    if(_set==nullptr)
    {
        return;
    }

    QSettings *set=_set;
    set->beginGroup("PlotConfig");
    set->setValue("LockAxis",lockAxis);
    set->setValue("Upper",upper);
    set->endGroup();
}

void SpectrumPlotWrap::GetPlotConfig(bool &lockAxis,double &upper)
{
    if(_set==nullptr)
    {
        return;
    }
    QSettings *set=_set;
    set->beginGroup("PlotConfig");
    lockAxis=set->value("LockAxis",false).toBool();
    upper=set->value("Upper",2000.0).toDouble();
    set->endGroup();
}


