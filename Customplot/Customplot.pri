INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/spectrumplotwrap.h \
    $$PWD/qcustomplot.h \
    $$PWD/DialogCustomPlot.h


SOURCES += \
    $$PWD/spectrumplotwrap.cpp \
    $$PWD/qcustomplot.cpp \
    $$PWD/DialogCustomPlot.cpp

FORMS += \
    $$PWD/DialogCustomPlot.ui
